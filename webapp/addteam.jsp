<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title> Create player </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="/css/addteam.css">
</head>
<body height="100%">
<div class="bg-img">
<div style="display:flex;">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        </div>
    </div>
</div>
<div class= "create">
<br>
<center><h1>Add New Team</h1></center>
<form:form action="createTeam" method="post" modelAttribute="teamInfo">  
<table style="margin:auto;">  
<tr><td>Name:</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="teamName" maxlength="30" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
<tr><td>Country:</td><td>  
<form:select path="country" style="width:155px">  
<option>INDIA</option>
<option>ENGLAND</option>
<option>PAKISTAN</option>
<option>WEST_INDIES</option>
<option>AUSTRALIA</option>
<option>SOUTH_AFRICA</option>
<option>SRI_LANKA</option>
</form:select>
</tr>
<tr></tr>
<tr><td colspan="2"><input type="submit" name= "action" value="add"/></td>
</table>  
</form:form>
</div>
<center>
    <button onclick="location.href='team?action=viewAll';"> back </button>
</center>
</div>
</body>
</html> 
