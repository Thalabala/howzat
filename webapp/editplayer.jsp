<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Edit player </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/editplayer.css">
    </head>
<body height="100%">
<div class="bg-img">
<div class="bg-img">
<div style="display:flex;">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
<div class= "create">
<br>
<center><h1>Update Player</h1></center>
<form:form action="updatePlayer" method="post" modelAttribute="playerInfo" enctype="multipart/form-data">  
<table style="margin:auto; ">
<form:input type="hidden" path="contactId" value="${playerInfo.contactId}"/>
<form:input type="hidden" path="id" value="${playerInfo.id}"/>
<input name="file" id="fileToUpload" type="file" />
<form:input type="hidden" path="displayPicture" value="${playerInfo.displayPicture}"/>
<tr><td>Name:</td><td><form:input type="text" path="name" value="${playerInfo.name}" required="required"/></td></tr>
<tr><td>Date Of Birth:</td><td><form:input type="Date" path="dateOfBirth" max="2000-01-02" min="1959-12-31" value="${playerInfo.dateOfBirth}" required="required"/></td></tr>
<tr><td>Country:</td><td>  
<form:select path="country" style="width:155px">  
<option> ${playerInfo.country} </option>
<option>INDIA</option>
<option>ENGLAND</option>
<option>PAKISTAN</option>
<option>WEST_INDIES</option>
<option>AUSTRALIA</option>
<option>SOUTH_AFRICA</option>
<option>SRI_LANKA</option>
</form:select>
</tr>
<tr><td>Player Role:</td><td>  
<form:select path="playerType" style="width: 155px"> 
<option> ${playerInfo.playerType} </option> 
<option>Batsman</option>
<option>Bowler</option>
<option>WicketKeeper</option>
<option>All-Rounder</option>
</form:select>
</tr>
<tr><td>Batting Type:</td><td>  
<form:select path="battingType" style="width:250px">
<option selected> ${playerInfo.battingType} </option>  
<option>Right-Hand batsman</option>
<option>left-Hand batsman</option>
</form:select>
</td></tr>  
<tr><td>Boweling Type:</td><td>  
<form:select path="bowelingType" style="width:250px">
<option selected> ${playerInfo.bowelingType} </option>  
<option>Right-Hand Fast Bowler</option>
<option>left-Hand Fast Bowler</option>
<option>Right-Hand Spin Bowler</option>
<option>left-Hand Spin Bowler</option>
<option>Right-Hand medium fast Bowler</option>
<option>Right-Hand medium fast Bowler</option>
</form:select>
</tr>  
<tr><td>Address:</td><td><form:input type="text" path="address" value="${playerInfo.address}"/></td></tr>
<tr><td>Contact Number:</td><td><form:input type="text" path="contactNumber" value="${playerInfo.contactNumber}"/></td></tr>
<tr><td>Pincode:</td><td><form:input type="text" path="pincode" value="${playerInfo.pincode}"/></td></tr>
</td></tr>
<tr><td colspan="2">
</table>
<center>
<button type="submit">update</button>
</form:form>
</center>  
</div>
<button onclick="location.href='viewPlayers';"> back </button>
</div>
</body>
</html> 
