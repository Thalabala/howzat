<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/viewmatch.css">
    <title>All Players</title>
</head>
<body>
    <div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
        <div>
            <a class="logo" href="home.jsp">
            <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
            </a>
        </div>
        <div class="header">
            <div class="header-right">
            <a class="active" href="home.jsp">Home</a>
            <a href="about.jsp">About</a>
            </div>
        </div>
    </div>
    <div>
    <form:form action="startNewOver" modelAttribute="startMatchInfo" method="post">
           <form:hidden path="bowlerId"/>
           <form:hidden path="battingTeamId"/>
           <form:hidden path="bowlingTeamId"/>
           <form:hidden path="bowlerName"/>
           <form:hidden path="matchId"/>
           <form:hidden path="overId"/>
           <form:hidden path="firstBatsmanId"/>
           <form:hidden path="secondBatsmanId"/>
           <form:hidden path="firstBatsmanName"/>
           <form:hidden path="secondBatsmanName"/>
           <form:hidden path="firstBatsmanScore"/>
           <form:hidden path="firstBatsmanFour"/>
           <form:hidden path="firstBatsmanSix"/>
           <form:hidden path="extras"/>
           <form:hidden path="secondBatsmanScore"/>
           <form:hidden path="secondBatsmanFour"/>
           <form:hidden path="secondBatsmanSix"/>
    <center>
        <h2> Bowling Team Players </h2>
    </center>
            <c:forEach var="player" items="${startMatchInfo.teamPlayers}">
                   <table>
                   <tr>
                    <td>   Name:   </td>
                    <td><c:out value="${player.name}" /></td>
                    <td>   Role:  </td>
                    <td><c:out value="${player.playerType}" /></td>
                    <td><input type="checkbox" name="bowler" value="${player.id}"></td>
                   </tr>
                   </table>
            </c:forEach>
            <button type="submit"> Resume Match </button>
    </form:form>
     </div>
</body>
</html>
