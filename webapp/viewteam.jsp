<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <meta charset="utf-8"/>
<html>
    <head>
        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <link rel="stylesheet" href="/css/viewteam.css">
        <title>All Players</title>
    </head>
    <body>
        <div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
            <div>
            <a class="logo" href="home.jsp">
            <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
            </a>
            </div>
            <div class="header">
                <div class="header-right">
                    <a class="active" href="home.jsp">Home</a>
                    <a href="about.jsp">About</a>
                </div>
            </div>
        </div>
        <div style=" ">
            <tr><td><button onclick="location.href='viewAllTeams';" title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;"><i class="fas fa-arrow-alt-circle-left"></i></button></td></tr><br>
        </div>
        <div align="center">
            <table border="1" cellpadding="5">
                <caption><h2> Team Details </h2></caption>
                        <tr><td> Team Id</td><td><c:out value="${teamInfo.id}" /></td>
                        <td> Team name</td><td><c:out value="${teamInfo.teamName}" /></td>
                        <td>Country</td><td><c:out value="${teamInfo.country}" /></td></tr>
            </table>
            <table border="1" cellpadding="5">
                <tr><td>Team Requirements</td><td>Required</td><td>Present in Team</td></tr>
                <tr><td> Batsmans</td><td>3</td><td><c:out value="${batsmanCount}" /></td></tr>
                <tr><td> Bowlers</td><td>3</td><td><c:out value="${bowlerCount}" /></td></tr>
                <tr><td>WicketKeepers</td><td>1</td><td><c:out value="${wicketkeeperCount}" /></td></tr>
            </table>
        </div>
        <br>
        <center>
            <h2> Team Players </h2>
        </center>
        <div class="container">
            <c:forEach var="player" items="${teamInfo.teamPlayers}"> 
            <div class="card">
                <div align="center">
                    <table>
                    <tr><td>id</td>
                    <td><c:out value="${player.id}" /></td></tr>
                    <tr><td>name</td>
                    <td><c:out value="${player.name}" /></td></tr>
                    <tr><td>date of birth</td>
                    <td><c:out value="${player.dateOfBirth}" /></td></tr>
                    <tr><td>country</td>
                    <td><c:out value="${player.country}" /></td></tr>
                    <tr><td>role</td>
                    <td><c:out value="${player.playerType}" /></td></tr>
                    </table>
                </div> 
            </div>
            </c:forEach>
        </div>
        <div>
            <center>
                <button onclick="location.href='editTeam?teamid=${teamInfo.id}';" title="Click to update player details">Edit</button>
                <button onclick="location.href='deleteTeam?teamid=${teamInfo.id}';" title="Click to delete player">Delete</button>
            </center>
        </div>
    </body>
</html>
