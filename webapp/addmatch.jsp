<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Create Match </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/addmatch.css">
    </head>
    <body height="100%">
        <div class="bg-img">
            <div style="display:flex;">
                <div>
                    <a class="logo" href="home.jsp">
                    <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
                    </a>
                </div>
                <div class="header">
                <div class="header-right">
                    <a class="active" href="home.jsp">Home</a>
                </div>
                </div>
            </div>
            <div class= "create">
            <br>
                <center><h1>Add New Match</h1></center>
                <form:form action="createMatch" method="post" modelAttribute="matchInfo">  
                <table style="margin:auto;">
                <tr><td>Match Name :</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="matchName" maxlength="30" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
                <tr><td>Location :</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="location" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
                <tr><td>Date:</td><td><input type="Date" name="matchDate" min="2019-07-23" required="required"/></td></tr>
                <tr><td>Match Type :</td><td>  
                <form:select path="matchType" style="width:155px">
                    <option>20 overs</option>
                    <option>50 overs</option>
                    <option>Test Match</option>
                </form:select>
                </tr>
                <tr><td colspan="2"><input type="submit"/></td>
                </table>  
                </form:form>
            </div>
                <center>
                <button name="action" onclick ="location.href='home.jsp'" type="submit"> back </button>
                </center>
        </div>
    </body>
</html> 
