<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/viewallteams.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
    <center>
        <h1> Existing Teams</h1>
    </center>
    <input type= "button" onclick ="location.href='newTeam'" title="Click to create new Team" class="block" value = "Add New Team" style="float:right;" />
    <tr><td><button onclick ="location.href='home.jsp'"  title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
    <br>
    <div class="container">
           <c:forEach var="team" items="${teams}"> 
           <div class="card">
             <button name="action" onclick="location.href='validateTeam?teamid=${team.id}';" style= "cursor:pointer; height:230px; width:200px">
             <div align="center">
                   <table>
                    <tr><td>Team Id</td>
                    <td><c:out value="${team.id}"/></td></tr>
                    <tr><td>Team Name</td>
                    <td><c:out value="${team.teamName}" /></td></tr>
                    <tr><td>Country</td>
                    <td><c:out value="${team.country}" /></td></tr>
                    <tr><td>Team Status</td>
                    <td><c:out value="${team.status}" /></td></tr>
                   </table>
                    </input>
             </div> </button>
             </div>
            </c:forEach>
    </div>
    <br>
    <br>  
    <center>
        <tr><td><input type= "button" class="block" onclick ="location.href='home.jsp'" value = "back"/></td></tr><br>
    </center>
</body>
</html>
