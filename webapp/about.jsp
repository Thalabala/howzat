<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Create User </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/adduser.css">
    </head>
    <body height="100%">
        <div class="bg-img">
            <div style="display:flex;">
                <div>
                    <a class="logo" href="home.jsp">
                    <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
                    </a>
                </div>
            <div class="header">
                <div class="header-right">
                    <a class="active" href="home.jsp">Home</a>
                </div>
            </div>
        </div>
        <div>
            <h2> Cricket </h2>
            <p> Cricket is a sport which is played between two teams of eleven players each who score runs (points). This is done by hitting the ball across the boundary, or by running between two sets of three small, wooden posts called wickets. The wickets are at each end of a rectangle of short grass called 'the pitch'. The pitch is inside a much larger oval of grass called the 'area of play'. The area of play is a 30 yard circle inside the cricket ground or stadium.[1]

The game started in England in the 16th century. The earliest definite reference to the sport is in a court case of 1598.[2] The court in Guildford heard a coroner, John Derrick, that when he was a scholar at the "Free School at Guildford", fifty years earlier, "he and diverse of his fellows did run and play [on the common land] at cricket and other players".[3][4] Later, the game spread to countries of the British Empire in the 19th and 20th centuries.

Today, it is a popular sport in England, Australia, India, Pakistan, Sri Lanka, Bangladesh, South Africa, New Zealand, the West Indies and several other countries such as Afghanistan, Ireland, Kenya, Scotland, the Netherlands and Zimbabwe.  </p>
            <h2> Cricket rules </h2>
            <p> There are two teams: The team bowling has 11 players on the field. The team batting has two players, one at each end of the wicket. Runs are got after a ball in bowled, by hitting the ball over the boundary, or by the umpires penalizing the bowling side for bowling an illegal ball.

The captain of the bowling team chooses a bowler from his team; the other 10 players are called 'fielders'. The bowler is trying to aim the ball at a wicket, which is made up of three sticks (called stumps) stuck into the earth, with two small sticks (called bails) balanced on them. One of the fielders, called the 'wicket keeper', stands behind the wicket to catch the ball if the bowler misses the wicket. The other fielders may chase the ball after the batsman has hit it.

The bowler runs towards his wicket, and bowls towards the batsman at the other wicket. He does not throw the ball. He bowls the ball overarm with a straight arm. If he bends his arm, the other teams are given one run and he has to bowl the ball again. An 'over' is six balls meaning he bowls six times. Then another player becomes the bowler for the next over, and bowls from the other end, and so on. The same bowler cannot bowl two overs one after the other.

The batsman is trying to defend the wicket from getting hit with the ball. He does this with a bat. When he hits the ball with his bat, he may run toward the other wicket. To score a run, the two batsmen must both run from their wicket to the other wicket, as many times as they can, before they can be run out. Being run out is explained below. If the ball leaves the field after being hit without bouncing, six runs are scored. If the ball rolls or bounces out, whether or not the batter hit it, it counts as four runs.

There are different ways that a batsman can get out. The most common ways are:

·        The batsman misses the ball and the ball hits the wicket: called bowled, or being "bowled out".

·        The ball hits the batsman's body when it would have hit the wicket otherwise. Called LBW (leg before wicket). The way this rule is applied is complicated; this is just the general idea.

·        A fielder catches the ball after the batsman hits it, and before it bounces or leaves the field: called caught.

·        While the batsmen are running, a fielder can throw the ball at the wicket. If the batsmen cannot finish the run in time, and the ball hits the wicket, the batsman nearer to the wicket that is hit is out: this is called run out.

When a batsman is out, another comes onto the field to take his place. The innings is over when ten wickets are taken (i.e. ten of the eleven batsmen are out). After this, the team which was the 'fielding' team becomes the 'batting' team. They now have to score more runs than the other team managed to score. If they score more runs before ten wickets are taken, they win. If they do not, the other team wins.

In a one-day game, each side has one innings, and innings are limited to a certain number of overs. In longer formats each side has two innings, and there is no specific limit to the number of overs in an innings.  </p>
        </div>
    </body>
</html>
