<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Create player </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/adduser.css">
    </head>
<body height="100%">
    <div class="bg-img">
        <div style="display:flex;">
            <div>
                <a class="logo" href="home.jsp">
                <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
                </a>
            </div>
        <div class="header">
            <div class="header-right">
                <a class="active" href="home.jsp">Home</a>
                <a href="about.jsp">About</a>
            </div>
        </div>
    </div>
    <div class= "create">
        <br>
        <center><h2>Add New Player</h2></center> 
        <form:form action="createPlayer" method="post" modelAttribute="playerInfo" enctype="multipart/form-data">
        <tr><td><table style="margin:auto;">
        <tr><td>Profile picture:</td><td><input name="file" id="fileToUpload" type="file"/></td></tr>
        <tr><td>Name:</td><td><form:input path="name" type="text" title="alphabets and upto 25 characters only"  required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
        <tr><td>Date Of Birth:</td><td><form:input path="dateOfBirth" type="Date" max="2000-01-02" min="1959-12-31" required="required" /></td></tr>
        <tr><td>Country:</td><td>  
        <form:select path="country" style="width:155px">  
            <option>INDIA</option>
            <option>ENGLAND</option>
            <option>PAKISTAN</option>
            <option>WEST_INDIES</option>
            <option>AUSTRALIA</option>
            <option>SOUTH_AFRICA</option>
            <option>SRI_LANKA</option>
        </form:select></tr>
        <tr><td>Player Role:</td><td>  
            <form:select path="playerType" style="width: 155px">  
                <option>Batsman</option>
                <option>Bowler</option>
                <option>WicketKeeper</option>
                <option>All-Rounder</option>
            </form:select></tr>
        <tr><td>Batting Type:</td><td>  
            <form:radiobutton path="battingType" value="Right Hand Batsman" checked="checked"/>Right Hand Batsman <br>
            <form:radiobutton path="battingType" value="Left Hand Batsman"/> Left Hand Batsman 
        </td></tr>
        <tr><td>Bowling Type:</td><td>
            <form:select path="bowelingType" style="width:250px">  
                <option>Right-Hand Fast Bowler</option>
                <option>left-Hand Fast Bowler</option>
                <option>Right-Hand Spin Bowler</option>
                <option>left-Hand Spin Bowler</option>
                <option>Right-Hand medium fast Bowler</option>
                <option>Right-Hand medium fast Bowler</option>
            </form:select>
        </tr>
        <tr><td>Address:</td><td><form:input type="text" placeholder="Door no, Street, city"    path="address"/></td></tr>
        <tr><td>Contact Number:</td><td><form:input type="tel" placeholder="10 numbers without +91" maxlength="10" path="contactNumber"/></td></tr>
        <tr><td>Pincode:</td><td><form:input type="tel" maxlength="6" path="pincode"/></td></tr>
</td></tr>
        <tr><td colspan="2"><input type="submit"/></td>
        </table>  
        </form:form>
    </div>
    <center>
        <button onclick="location.href='viewPlayers';"> back </button>
    </center>
    </div>
</body>
</html>
