<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<title> Index </title>
<link rel="stylesheet" href="/css/index.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="picture">
<div class="logo">
<h1> Welcome to Howzat </h1>
</div>
</div>
<div class="wording">
<p style="margin-top:40%;">
<div style="float:right;"> 
    <button onclick="location.href='logoutUser';" type="submit">
    <span class="glyphicon glyphicon-log-out"></span>Log out</button>
</div>
<button class="block" onclick="location.href='viewPlayers';">Player Management</button>
<br>
<button class="block" onclick="location.href='viewAllTeams';">Team Management</button>
<br>
<button class="block" onclick="location.href='viewAllMatches';"> Match Management </button>
</div>
</body>
</html> 
