<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/addteamtomatch.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
    <center>
        <h1> Existing Teams</h1>
    </center>
    <form:form action="addTeamToMatch" method="post">
    <input type="hidden" name="matchid" value="${matchInfo.id}"/>
        <tr><td><button name="action" value="viewall" type="submit" title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2> Team Details </h2></caption>
                    <tr><td>Match Name</td><td><c:out value="${matchInfo.matchName}" /></td></tr>
                    <tr><td>Location</td><td><c:out value="${matchInfo.location}" /></td></tr>
                    <tr><td>Date</td><td><c:out value="${matchInfo.date}" /></td></tr>
                    <tr><td>Match Type</td><td><c:out value="${matchInfo.matchType}" /></td></tr>
         </table>
    </div>
    <br>
    
    <div class="container">
            <c:forEach var="team" items="${matchInfo.teamsForMatch}"> 
           <div class="card">
             <div align="center">
             <input type="checkbox" name="selectedteams" value="${team.id}"> click to add Team<br>
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${team.id}" /></td></tr>
                    <tr><td>Team Name</td>
                    <td><c:out value="${team.teamName}" /></td></tr>
                    <tr><td>Team Country</td>
                    <td><c:out value="${team.country}" /></td></tr>
                    <tr><td>Team Status</td>
                    <td><c:out value="${team.status}" /></td></tr>
                    </table>
                </div> 
             </div>
            </c:forEach>
    </div>
    <br>
    <br>  
    <center>
    <footer>
       <button type="submit" style= "cursor:pointer; font-size:24px; margin-right:3%;"><i class="far fa-save"></i></button>
        <tr><td><button name="action" value="viewAll" type="submit" title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
    </footer>
    </center>
    </form:form>
</body>
</html>
