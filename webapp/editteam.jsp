<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/editteam.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
<div>
<div>
<tr><td><button onclick="location.href='team?action=viewAll';" title="Back" style=" cursor:pointer; margin-left:3%;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
<form action="updateTeam" method="post">
<button name="action" value="update" title="Click to update player details" type="submit" style="float:right;"> Update </button>
    <center>
        <h1> Created Team </h1>
    </center>        
</div>      
    <div align="center">
        <table style="margin:auto;">  
        <tr><td>Name:</td><td><input type="text" title="alphabets and upto 25 characters only" name="name" value="${teamInfo.teamName}" maxlength="30" required pattern="[A-Z][a-z]*{25}" /></td></tr>
    <tr><td>Country:</td><td>  
    <select name="country" style="width:155px" readonly>  
    <option>${teamInfo.country}</option>
         </table>
    </div>
    <br>
    <div>
    <div class="container">
    <center>
        <h2> Team Players </h2>
    </center>
            <c:forEach var="player" items="${teamInfo.teamPlayers}"> 
           <div class="card">
             <div align="center">
             <input type="checkbox" name="removeplayers" value="${player.id}"> click to remove player<br>
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${player.id}" /></td></tr>
                    <tr><td>name</td>
                    <td><c:out value="${player.name}" /></td></tr>
                    <tr><td>date of birth</td>
                    <td><c:out value="${player.dateOfBirth}" /></td></tr>
                    <tr><td>country</td>
                    <td><c:out value="${player.country}" /></td></tr>
                    <tr><td>role</td>
                    <td><c:out value="${player.playerType}" /></td></tr>
                    </table>
                </div>
             </div>
            </c:forEach>
            <center>
    </div>     
    <div class="container">
    <center>
        <h2> Players not in Team </h2>
    </center>
            <c:forEach var="player" items="${teamInfo.players}"> 
           <div class="card">
             <div align="center">
             <input type="checkbox" name="addplayers" value="${player.id}"> click to add player<br>
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${player.id}" /></td></tr>
                    <tr><td>name</td>
                    <td><c:out value="${player.name}" /></td></tr>
                    <tr><td>date of birth</td>
                    <td><c:out value="${player.dateOfBirth}" /></td></tr>
                    <tr><td>country</td>
                    <td><c:out value="${player.country}" /></td></tr>
                    <tr><td>role</td>
                    <td><c:out value="${player.playerType}" /></td></tr>
                    </table>
                </div>
             </div>
            </c:forEach>
     </div>
    </div>
    </div>
        <div>
            <center>
            <input type="hidden" name="teamid" value="${teamInfo.id}"/>
            <button title="Click to update player details" type="submit"> Update </button>
            </form>
            <button onclick="location.href='viewAllTeams';"> Cancel </button>
            </center>
        </div>
</body>
</html>
