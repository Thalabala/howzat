<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/viewmatch.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
<div>
<tr><td><button onclick="location.href='viewAllMatches';" title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
</div>       
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2> match Details </h2></caption>
                    <tr><td> Id</td><td><c:out value="${matchInfo.id}" /></td>
                    <td>Match Name</td><td><c:out value="${matchInfo.matchName}" /></td>
                    <td>location</td><td><c:out value="${matchInfo.location}" /></td>
                    <td>date</td><td><c:out value="${matchInfo.date}" /></td>
                    <td>matchType</td><td><c:out value="${matchInfo.matchType}" /></td></tr>
         </table>
    </div>
    <br>
    <center>
        <h2> Teams </h2>
    </center>
    <div class="container">
    <center>
            <c:forEach var="team" items="${matchInfo.teamsSelectedForMatch}"> 
           <div class="card">
             <div align="center">
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${team.id}" /></td></tr>
                    <tr><td>Team Name</td>
                    <td><c:out value="${team.teamName}" /></td></tr>
                    <tr><td>Country</td>
                    <td><c:out value="${team.country}" /></td></tr>
                    <tr><td>Team Status</td>
                    <td><c:out value="${team.status}" /></td></tr>
                    </table>
                </div> 
             </div>
            </c:forEach>
    </center>
    </div>
            <center>
            <button onclick="location.href='editMatch?matchId=${matchInfo.id}';" title="Click to update match details">Edit</button>
            <button onclick="location.href='deleteMatch?matchId=${matchInfo.id}';" title="Click to delete match" >Delete</button>
            <button onclick="location.href='startMatch?matchId=${matchInfo.id}';" title="Click to play match" >Play</button>
            </center>
</body>
</html>
