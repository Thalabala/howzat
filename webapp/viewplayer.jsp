<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<link rel="stylesheet" href="/css/viewplayer.css">
    <title> view player </title>
</head>
<body>
<div style="display:flex; background-color: #00FFFF;">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
        
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2> Player Details </h2></caption>
                    <tr><td colspan=2 align="center"><img src= "${playerInfo.displayPicture}" style="width:150px; height:150px; border-radius:50%;"/></td></tr>
                    <tr><td> id</td><td><c:out value="${playerInfo.id}" /></td></tr>
                    <tr><td>name</td><td><c:out value="${playerInfo.name}" /></td></tr>
                    <tr><td> Age </td><td><c:out value="${playerInfo.age}" /></td></tr>
                    <tr><td> Date of birth</td><td><c:out value="${playerInfo.dateOfBirth}" /></td></tr>
                    <tr><td> Country</td><td><c:out value="${playerInfo.country}" /></td></tr>
                    <tr><td> Role</td><td><c:out value="${playerInfo.playerType}" /></td></tr>
                    <tr><td> Batting style</td><td><c:out value="${playerInfo.battingType}" /></td></tr>   
                    <tr><td>Boweling type</td><td><c:out value="${playerInfo.bowelingType}" /></td></tr>
                    <tr><td> address</td><td><c:out value="${playerInfo.address}" /></td></tr>
                    <tr><td>    contact number</td><td><c:out value="${playerInfo.contactNumber}" /></td></tr>
                    <tr><td> pincode</td><td><c:out value="${playerInfo.pincode}" /></td></tr>                 
        </table>
        <br>
        <input type= "button" onclick ="location.href='newPlayer'" class="block" value = "Add New Player"/>
        <br>
        <br> 
            <button onclick="location.href='editPlayer?id=${playerInfo.id}';">Edit</button>
            &nbsp;&nbsp;
            <button onclick="location.href='deletePlayer?id=${playerInfo.id}';" title="Click to delete player" >Delete</button>
        <center>
        <button onclick="location.href='viewPlayers';"> back </button>
        </center>
    </div>   
</body>
</html>
