<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/viewmatch.css">
    <title>All Players</title>
</head>
<body>
    <div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
        <div>
            <a class="logo" href="home.jsp">
            <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
            </a>
        </div>
        <div class="header">
            <div class="header-right">
            <a class="active" href="home.jsp">Home</a>
            <a href="about.jsp">About</a>
            </div>
        </div>
    </div>
    <div style="display:flex; width:max-width; height:100%;">
        <div style="display:inline-block; width:50%; height:100%;">
            <div style="height:50%;">
            <center><button style="display:block;" id="playMatch" onclick="playBallAjax(${startMatchInfo.firstBatsmanId}, ${startMatchInfo.bowlerId}, ${startMatchInfo.matchId}, ${startMatchInfo.overId})"> Play </button></center>
            </div>
           <form:form action="changePlayer" method="post" modelAttribute="startMatchInfo">
           <form:hidden path="bowlerId"/>
           <form:hidden path="battingTeamId"/>
           <form:hidden path="overId"/>
           <form:hidden path="bowlingTeamId"/>
           <form:hidden path="bowlerName"/>
           <form:hidden path="matchId"/>
           <input type="hidden" name="choosePlayer" id="choosePlayer"  value="0"/>
           <form:input type="hidden" path="firstBatsmanId" id="firstBatsmanIdToPojo"  value="${startMatchInfo.firstBatsmanId}"/>
           <form:input type="hidden" path="secondBatsmanId" id="secondBatsmanIdToPojo"  value="${startMatchInfo.secondBatsmanId}"/>
           <form:input type="hidden" path="firstBatsmanName" id="firstBatsmanNameToPojo"  value="${startMatchInfo.firstBatsmanName}"/>
           <form:input type="hidden" path="secondBatsmanName" id="secondBatsmanNameToPojo"  value="${startMatchInfo.secondBatsmanName}"/>
           <form:input type="hidden" path="firstBatsmanScore" id="firstBatsmanScoreToPojo"  value="${startMatchInfo.firstBatsmanScore}"/>
           <form:input type="hidden" path="firstBatsmanFour" id="firstBatsmanFourToPojo"  value="${startMatchInfo.firstBatsmanFour}"/>
           <form:input type="hidden" path="firstBatsmanSix" id="firstBatsmanSixToPojo"  value="${startMatchInfo.firstBatsmanSix}"/>
           <form:input type="hidden" path="extras" id="extrasToPojo" value="${startMatchInfo.extras}" />
           <form:input type="hidden" path="secondBatsmanScore" id="secondBatsmanScoreTopojo"  value="${startMatchInfo.secondBatsmanScore}"/>
           <form:input type="hidden" path="secondBatsmanFour" id="secondBatsmanFourToPojo"  value="${startMatchInfo.secondBatsmanFour}"/>
           <form:input type="hidden" path="secondBatsmanSix" id="secondBatsmanSixToPojo"  value="${startMatchInfo.secondBatsmanSix}"/>
           <div style="display:none;" id="changeBowler"> 
            <center>
            <button type="submit" > Select Next Bowler </button>
            </center>
            </div>
            <div style="display:none;" id="changeBatsman">
            <button type="submit" > Select Next Batsman </button>
            </div>
            </form:form>
            
            <div style="height:50%;">
                <center><span id="deliveryResult">  </span></center>
            </div>
        </div>
        <div style="display:inline-block; width:50%; height:100%;">
         score area
            <div style="height:50%;">
                <table style="width:50%; height:25%; background-color:#0DFFFF;">
                <input type="hidden" value="${startMatchInfo.firstBatsmanId}" id="strickerId">
                <tr><td> ID </td><td colspan="2"> Batsman Name</td><td> Runs </td><td> Fours </td><td> Sixes </td> <td> Extras </td></tr>
                <tr><td id="firstBatsmanId">${startMatchInfo.bowlingTeamId}</td><td id="firstBatsmanName">${startMatchInfo.firstBatsmanName}</td><td>*</td><td id="firstBatsmanScore">${startMatchInfo.firstBatsmanScore}</td><td id="firstBatsmanFour">0</td><td id="firstBatsmanSix">0</td><td id="extras">${startMatchInfo.extras}</td></tr>
                <tr><td id="secondBatsmanId">${startMatchInfo.secondBatsmanId}</td><td id="secondBatsmanName">${startMatchInfo.secondBatsmanName}</td><td></td><td id="secondBatsmanScore">${startMatchInfo.secondBatsmanScore}</td><td id="secondBatsmanFour">0</td><td id="secondBatsmanSix">0</td></tr>
                </table>
            </div>
            <div style="height:50%">
                <table border="1" style="width:50%; height:25%;">
                <tr id="bowlerFirstRow"><td> Bowler Name </td></tr>
                <tr id="bowlerSecondRow"><td>${startMatchInfo.bowlerName}</td></tr>
                </table>
            </div>                  
        </div>
    </div>
<script>
function playBallAjax(batsmanId, bowlerId, matchId, overId) {
httpRequest = new XMLHttpRequest();
var strickerId = document.getElementById('strickerId').value;
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', 'playBall?strickerId='+strickerId+'&bowlerId='+bowlerId+'&matchId='+matchId+'&overId='+overId);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            var firstBatsmanId = document.getElementById('firstBatsmanId').innerHTML;
            var scoreCard = httpRequest.response;
            var firstBatsmanName = document.getElementById('firstBatsmanName').innerHTML;
            var secondBatsmanName = document.getElementById('secondBatsmanName').innerHTML;
            var firstBatsmanScore = document.getElementById('firstBatsmanScore').innerHTML;
            var secondBatsmanScore = document.getElementById('secondBatsmanScore').innerHTML;
            var firstBatsmanFour = document.getElementById('firstBatsmanFour').innerHTML;
            var secondBatsmanFour = document.getElementById('secondBatsmanFour').innerHTML;
            var firstBatsmanSix = document.getElementById('firstBatsmanSix').innerHTML;
            var secondBatsmanSix = document.getElementById('secondBatsmanSix').innerHTML;
            var firstRow = document.createElement("td");
            var secondScore = document.createElement("td");
            var extras = document.getElementById("extras").innerHTML;
            var deliveryResultShow = document.getElementById('deliveryResult');
            var firstBatsmanFour = document.getElementById("firstBatsmanFour").innerHTML;
            var firstBatsmanSix = document.getElementById("firstBatsmanSix").innerHTML;
            var runs = scoreCard.runs;
            var ballNo = scoreCard.ballNo;
            var deliveryResult = scoreCard.deliveryResult;
            var changeBowler = document.getElementById('changeBowler');
            var changeBatsman = document.getElementById('changeBatsman');
            var playMatchButton = document.getElementById("playMatch");
            if(6 == runs) {
                document.getElementById("firstBatsmanSix").innerHTML = (firstBatsmanSix * 1) + 1;
                document.getElementById('firstBatsmanScore').innerHTML = (firstBatsmanScore * 1) + runs;
            } else if(4 == runs) {
                document.getElementById("firstBatsmanFour").innerHTML = (firstBatsmanFour * 1) + 1;
                document.getElementById('firstBatsmanScore').innerHTML = (firstBatsmanScore * 1) + runs;
            } else if(deliveryResult == "Wide" ||
                     deliveryResult == "No_Ball" || deliveryResult == 
                     "Byes" || deliveryResult == "Leg_Byes" ) {
                document.getElementById("extras").innerHTML = (extras * 1) + 1;           
            } else if((1 == runs) || (3 == runs) || (5 == runs)) {
                var tempId = document.getElementById('secondBatsmanId').innerHTML;
                var tempName = document.getElementById('secondBatsmanName').innerHTML;
                var tempRuns = document.getElementById('secondBatsmanScore').innerHTML;
                var tempFour = document.getElementById('secondBatsmanFour').innerHTML;
                var tempSix = document.getElementById('secondBatsmanSix').innerHTML;  
                firstBatsmanScore = (document.getElementById('firstBatsmanScore').innerHTML * 1) + runs;
                document.getElementById('secondBatsmanName').innerHTML = firstBatsmanName;
                document.getElementById('firstBatsmanName').innerHTML = tempName;
                document.getElementById('secondBatsmanScore').innerHTML = firstBatsmanScore;
                document.getElementById('firstBatsmanScore').innerHTML = tempRuns;
                document.getElementById('secondBatsmanFour').innerHTML = firstBatsmanFour;
                document.getElementById('firstBatsmanFour').innerHTML = tempFour;
                document.getElementById('secondBatsmanSix').innerHTML = firstBatsmanSix;
                document.getElementById('firstBatsmanSix').innerHTML = tempSix;
                document.getElementById('secondBatsmanId').innerHTML = firstBatsmanId;
                document.getElementById('firstBatsmanId').innerHTML = tempId;
                document.getElementById('strickerId').value = tempId;
            } else {
                document.getElementById('firstBatsmanScore').innerHTML = (firstBatsmanScore * 1) + scoreCard.runs;
            }
            firstBatsmanScore.innerHTML = "" + scoreCard.runs + "";
            firstRow.innerHTML = ""+ scoreCard.ballNo +"";
            bowlerFirstRow.appendChild(firstRow);
            secondScore.innerHTML = ""+ scoreCard.runs +"";
            bowlerSecondRow.appendChild(secondScore);
            deliveryResultShow.innerHTML = ""+ scoreCard.deliveryResult +"";
            if(6 == ballNo || deliveryResult == "Wicket") {
                document.getElementById("firstBatsmanIdToPojo").value = document.getElementById('firstBatsmanId').innerHTML;
                document.getElementById("secondBatsmanIdToPojo").value = document.getElementById('secondBatsmanId').innerHTML;
                document.getElementById("firstBatsmanNameToPojo").value = document.getElementById('firstBatsmanName').innerHTML;
                document.getElementById("secondBatsmanNameToPojo").value = document.getElementById('secondBatsmanName').innerHTML;
                document.getElementById("firstBatsmanScoreToPojo").value = document.getElementById('firstBatsmanScore').innerHTML;
                document.getElementById("firstBatsmanFourToPojo").value =document.getElementById('firstBatsmanFour').innerHTML;
                document.getElementById("firstBatsmanSixToPojo").value = document.getElementById('firstBatsmanSix').innerHTML;
                document.getElementById("extrasToPojo").value = document.getElementById("extras").innerHTML;
               /* document.getElementById("secondBatsmanScoreToPojo").value = document.getElementById('secondBatsmanScore').innerHTML;*/
                document.getElementById("secondBatsmanFourToPojo").value = document.getElementById('secondBatsmanFour').innerHTML;
                document.getElementById("secondBatsmanSixToPojo").value = document.getElementById('secondBatsmanSix').innerHTML;
                if(6 == ballNo) {
                    document.getElementById("choosePlayer").value = 0;
                    changeBowler.style.display= "block";
                    playMatchButton.style.display= "none";
                } else if(deliveryResult == "Wicket") {
                    document.getElementById("choosePlayer").value = 1;
                    changeBatsman.style.display= "block";
                    playMatchButton.style.display= "none";
                }
            }
        } else {
            console.log('Something went wrong..!!');
        }
        }   
    }
    }
</script>
</body>
</html>
