<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Create User </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/adduser.css">
    </head>
    <body height="100%">
        <div class="bg-img">
        <div style="display:flex;">
            <div>
                <a class="logo" href="home.jsp">
                <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
                </a>
            </div>
        <div class="header">
            <div class="header-right">
                <a class="active" href="home.jsp">Home</a>
                <a href="about.jsp">About</a>
            </div>
        </div>
    </div>
        <div class= "create">
        <br>
            <center><h2>Add New User</h2></center>
            <form:form action="createUser" method="post" modelAttribute="userInfo">  
            <table style="margin:auto;">
            <tr><td>Name:</td><td><input type="text" title="alphabets and upto 25 characters only" name="name" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
            <tr><td>Email Id:</td><td><form:input type="email" path="emailId" required="required"/></td></tr>
            <tr><td>Role:</td><td>  
            <form:select path="role" style="width:155px">  
                <option>ADMIN</option>
            </form:select>
            </tr>
            <tr><td>Gender:</td><td>  
            <form:radiobutton path="gender" value="male" checked="checked"/>Male<br>   
            <form:radiobutton path="gender" value="female"/>Female<br>
            <form:radiobutton path="gender" value="transgender"/>Transgender
            </td></tr>  
            </tr> 
            <tr><td>Contact Number:</td><td><form:input type="tel" placeholder="10 numbers without +91" maxlength="10" path="contactNumber"/></td></tr>
            <tr><td>password:</td><td><form:input type="password" minlength="6" path="password"/></td></tr> 
            </td></tr>
            <tr></tr>
            <tr></tr>
            <tr><td colspan="2"><input type="submit"/></td>
            </table>  
            </form:form>
        </div>
    </body>
</html> 
