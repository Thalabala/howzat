<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8"/>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/editmatch.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
<div>
<button onclick="location.href='viewAllMatches';" title="Back" style=" cursor:pointer; margin-left:3%;" ><i class="fas fa-arrow-alt-circle-left"></i> </button><br>
<form:form action="updateMatch" method="post" modelAttribute="matchInfo">
<form:hidden path="id"/>
<button title="Click to update match details" type="submit" style="float:right;"> Update </button>
    <center>
        <h1> Edit Match </h1>
    </center>
</div>      
    <div align="center">
        <table style="margin:auto;">  
    <tr><td>Match Name:</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="matchName" value="${matchInfo.matchName}" maxlength="30" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
    <tr><td>location:</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="location" value="${matchInfo.location}" maxlength="30" required="required" pattern="[A-Z][a-z]*{25}" /></td></tr>
    <tr><td>date:</td><td><input type="text" name="matchDate" value="${matchInfo.date}" readonly="readonly"/></td></tr>
    <tr><td>matchType:</td><td><form:input type="text" title="alphabets and upto 25 characters only" path="matchType" value="${matchInfo.matchType}" readonly="readonly"/></td></tr>
         </table>
    </div>
    <br>
    <center>
        <h2> Match Teams </h2>
    </center>
    <div class="container">
            <c:forEach var="team" items="${matchInfo.teamsSelectedForMatch}"> 
           <div class="card">
             <div align="center">
             <input type="checkbox" name="removeteams" value="${team.id}"> click to remove player<br>
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${team.id}" /></td></tr>
                    <tr><td>Team Name</td>
                    <td><c:out value="${team.teamName}" /></td></tr>
                    <tr><td>Country</td>
                    <td><c:out value="${team.country}" /></td></tr>
                    <tr><td>Team Status</td>
                    <td><c:out value="${team.status}" /></td></tr>
                    </table>
                </div>
             </div>
            </c:forEach>
            <center>
    </div>     
   <center>
        <h2> Teams not in match  </h2>
    </center>
    <div class="container">
            <c:forEach var="team" items="${matchInfo.teamsForMatch}"> 
           <div class="card">
             <div align="center">
             <input type="checkbox" name="addteams" value="${team.id}"> click to add player<br>
                   <table>
                    <tr><td>id</td>
                    <td><c:out value="${team.id}" /></td></tr>
                    <tr><td>Team Name</td>
                    <td><c:out value="${team.teamName}" /></td></tr>
                    <tr><td>Country</td>
                    <td><c:out value="${team.country}" /></td></tr>
                    <tr><td>Team Status</td>
                    <td><c:out value="${team.status}" /></td></tr>
                    </table>
                </div>
             </div>
            </c:forEach>
     </div>
            <center>
            <button title="Click to update match details" type="submit"> Update </button>
    </form:form>
            <button onclick="location.href='viewAllMatches';"> Cancel </button>
            </center>
    </div>
</body>
</html>
