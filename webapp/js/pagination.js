function callAjax(page, direction) {
httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    if (0 == direction) {
        document.getElementById('currentPage').value=page;
        currentPage = page;
    } else if (1 == direction){
        currentPage = document.getElementById('currentPage').value;
        currentPage = currentPage - 1;
        document.getElementById('currentPage').value=currentPage;
    } else if (2 == direction){
        currentPage = document.getElementById('currentPage').value;
        currentPage = currentPage * 1 + 1;
        document.getElementById('currentPage').value=currentPage;
    }
    httpRequest.open('GET', 'viewPlayersPageWise?page='+currentPage);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        document.getElementsByClassName('loading').innerHTML = "";
        if (httpRequest.status === 200) {
            var array = httpRequest.response;
            var playerInfo = document.getElementsByClassName('playerInfo');
            var viewId = document.getElementsByClassName('viewid');
            var card = document.getElementsByClassName("card");
            for(var i=0; i < array.length; i++) {
                playerInfo[i].style.visibility = "visible";
                card[i].style.display = "inline-block";
                playerInfo[i].innerHTML="<pre>"+ array[i].id +"\n"+ array[i].name 
                    +"\n"+ array[i].dateOfBirth +"\n"+ array[i].country +"\n"
                    + array[i].playerType + "</pre>";
                viewId[i].value = array[i].id;
                playerInfo[i].innerHTML="<html><body><table><tr><td>id</td><td>"
                    + array[i].id +"</td></tr><tr><td>name</td><td>" + array[i].name 
                    +"</td></tr><tr><td>date of birth</td><td>"+array[i].dateOfBirth
                    +"</td></tr><tr><td>country</td><td>"+array[i].country
                    +"</td></tr><tr><td>role</td><td>"+array[i].playerType
                    +"</td></tr></table></body></html>";
             }
            for(var j = array.length; j <= 5; j++){
                playerInfo[j].style.visibility = "hidden";
                card[j].style.display = "none";
            }
        } else {
            console.log('Something went wrong..!!');
        }
        }   
    }
    }
