<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<link rel="stylesheet" href="/css/viewallplayers.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
    <center>
        <h1> Existing Players</h1>
    </center>
    <center>
    <input type= "button" onclick ="location.href='newPlayer'" title="Click to create new player" class="block" value = "Add New Player"/>
    </center>
    <br>
    <div class="container">
           <c:forEach var="player" items="${playerPaginationInfo.players}"> 
           <div class="card">
             <form action="viewPlayer" method="post">
             <input type="hidden" name="id" class="viewid" value="${player.id}">
             <button type="submit" action="viewPlayer" class="playerInfo" style= "cursor:pointer; height:230px; width:200px">
             <div class="viewid">
                    <table>
                    <tr><td>id</td>
                    <td><c:out value="${player.id}" /></td></tr>
                    <tr><td>name</td>
                    <td><c:out value="${player.name}" /></td></tr>
                    <tr><td>date of birth</td>
                    <td><c:out value="${player.dateOfBirth}" /></td></tr>
                    <tr><td>country</td>
                    <td><c:out value="${player.country}" /></td></tr>
                    <tr><td>role</td>
                    <td><c:out value="${player.playerType}" /></td></tr>
                    </table>
                    </input>
             </div></button>
             </form>
             </div>
            </c:forEach>
    </div>
    <div>
    <div style="float:left;">
        <button id="currentPage" onclick="callAjax(${currentPage}, 1);">Previous</button>
    </div>
    <center>
    <table border="1" cellpadding="5" cellspacing="5">
        <tr>
            <c:forEach begin="1" end="${playerPaginationInfo.noOfPages}" var="page">
                <c:choose>
                    <c:when test="${currentPage eq page}">
                        <td><button onclick="callAjax(${page},0);">${page}</button></td>
                    </c:when>
                    <c:otherwise>
                        <td><button onclick="callAjax(${page},0);">${page}</button></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>
    </table>
    </center>
    <div style="float:right;">
    <c:if test="${currentPage lt playerPaginationInfo.noOfPages}">
        <button onclick="callAjax(${currentPage}, 2);">Next</button>
    </c:if>
    </div>
    </div>
    <br>
    <br>
<script >
function callAjax(page, direction) {
httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    if (0 == direction) {
        document.getElementById('currentPage').value=page;
        currentPage = page;
    } else if (1 == direction){
        currentPage = document.getElementById('currentPage').value;
        currentPage = currentPage - 1;
        document.getElementById('currentPage').value=currentPage;
    } else if (2 == direction){
        currentPage = document.getElementById('currentPage').value;
        currentPage = currentPage * 1 + 1;
        document.getElementById('currentPage').value=currentPage;
    }
    httpRequest.open('GET', 'viewPlayersPageWise?page='+currentPage);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        document.getElementsByClassName('loading').innerHTML = "";
        if (httpRequest.status === 200) {
            var array = httpRequest.response;
            var playerInfo = document.getElementsByClassName('playerInfo');
            var viewId = document.getElementsByClassName('viewid');
            var card = document.getElementsByClassName("card");
            for(var i=0; i < array.length; i++) {
                playerInfo[i].style.visibility = "visible";
                card[i].style.display = "inline-block";
                playerInfo[i].innerHTML="<pre>"+ array[i].id +"\n"+ array[i].name 
                    +"\n"+ array[i].dateOfBirth +"\n"+ array[i].country +"\n"
                    + array[i].playerType + "</pre>";
                viewId[i].value = array[i].id;
                playerInfo[i].innerHTML="<html><body><table><tr><td>id</td><td>"
                    + array[i].id +"</td></tr><tr><td>name</td><td>" + array[i].name 
                    +"</td></tr><tr><td>date of birth</td><td>"+array[i].dateOfBirth
                    +"</td></tr><tr><td>country</td><td>"+array[i].country
                    +"</td></tr><tr><td>role</td><td>"+array[i].playerType
                    +"</td></tr></table></body></html>";
             }
            for(var j = array.length; j <= 5; j++){
                playerInfo[j].style.visibility = "hidden";
                card[j].style.display = "none";
            }
        } else {
            console.log('Something went wrong..!!');
        }
        }   
    }
    }
    </script>
    <center>    
        <tr><td><input type= "button" class="block" onclick ="location.href='home.jsp'" value = "back"/></td></tr><br>
    </center>
</body>
</html>
