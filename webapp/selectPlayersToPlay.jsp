<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8"/>
<html>
<head>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="/css/editteam.css">
    <title>Team Players for Match</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;">
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
    <br>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2> match Details </h2></caption>
                    <tr><td> Id</td><td>Match Name</td><td>location</td>
                    <td><center>Date</center></td><td>Match Type</td></tr>
                    <tr><td><c:out value="${playMatchInfo.matchInfo.id}" /></td>
                    <td><c:out value="${playMatchInfo.matchInfo.matchName}" /></td>
                    <td><c:out value="${playMatchInfo.matchInfo.location}" /></td>
                    <td><c:out value="${playMatchInfo.matchInfo.date}" /></td>
                    <td><c:out value="${playMatchInfo.matchInfo.matchType}" /></td></tr>
         </table>
    </div>
    <form:form action="savePlayers" method="post">
    <div>
    <div style="display:inline-block; margin-left:20%;">
    <center>
        <h2> Batting Team Players </h2>
    </center>
            <c:forEach var="player" items="${playMatchInfo.battingTeamInfo.teamPlayers}">
                   <table>
                   <tr>
                    <td>   Name:   </td>
                    <td><c:out value="${player.name}" /></td>
                    <td>   Role:  </td>
                    <td><c:out value="${player.playerType}" /></td>
                    <td><input type="checkbox" name="batsmans" value="${player.id}"></td>
                   </tr>
                    </table>
            </c:forEach>
    </div>     
    <div style="display:inline-block; margin-left:20%;">
    <center>
        <h2> Bowling Team Players </h2>
    </center>
            <c:forEach var="player" items="${playMatchInfo.bowlingTeamInfo.teamPlayers}">
                   <table>
                   <tr>
                    <td>   Name:   </td>
                    <td><c:out value="${player.name}" /></td>
                    <td>   Role:  </td>
                    <td><c:out value="${player.playerType}" /></td>
                    <td><input type="checkbox" name="bowler" value="${player.id}"></td>
                   </tr>
                   </table>
            </c:forEach>
     </div>
     <div>
            <center>
            <input type="hidden" name="matchId" value="${playMatchInfo.matchInfo.id}"/>
            <button title="Click to start match" type="submit"> Start </button>
            </form:form>
            <button onclick="location.href='viewAllMatches';"> Cancel </button>
            </center>
    </div>
</body>
</html>
