<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="/css/viewallmatches.css">
    <title>All Players</title>
</head>
<body>
<div style="display:flex; background-color: linear-gradient(to left, #00ffff 12%, #ffccff 84%);">
    <div>
        <a class="logo" href="home.jsp">
        <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
        </a>
    </div>
    <div class="header">
        <div class="header-right">
        <a class="active" href="home.jsp">Home</a>
        <a href="about.jsp">About</a>
        </div>
    </div>
</div>
    <center>
        <h1> Existing Matches</h1>
    </center>
    <input type= "button" onclick ="location.href='newMatch';" title="Click to create new Match" class="block" value = "Add New Matches" style="float:right;" />
    <tr><td><button onclick="location.href='home.jsp';" title="Back" style=" cursor:pointer; margin-left:3%; font-size:24px;" ><i class="fas fa-arrow-alt-circle-left"></i> </button></td></tr><br>
    <br>
    <div class="container">
            <c:forEach var="match" items="${matches}"> 
           <div class="card">
             <button onclick="location.href='viewMatch?matchId=${match.id}';" style= "cursor:pointer; height:230px; width:200px">
             <div align="center">
                   <table>
                    <tr><td>Id</td>
                    <td><c:out value="${match.id}" /></td></tr>
                    <tr><td>Match Name</td>
                    <td><c:out value="${match.matchName}" /></td></tr>
                    <tr><td>location</td>
                    <td><c:out value="${match.location}"  /></td></tr>
                    <tr><td>date</td>
                    <td><c:out value="${match.date}" /></td></tr>
                    <tr><td>matchType</td>
                    <td><c:out value="${match.matchType}" /></td></tr>
                   </table>
                    </input>
             </div> </button>
             </div>
            </c:forEach>
    </div>
    <br>
    <br>  
    <center>
        <tr><td><input type= "button" class="block" onclick ="location.href='home.jsp'" value = "back"/></td></tr><br>
    </center>
</body>
</html>
