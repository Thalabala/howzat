<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title> Login to Howzat </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/adduser.css">
    </head>
    <body height="100%">
        <div class="bg-img">
            <div style="display:flex;">
                <div>
                    <a class="logo" href="index.jsp">
                    <img src="/css\images\logo.png" style="width:70px; height:60px; margin-top:0;" >
                    </a>
                </div>
                <div class="header">
                    <div class="header-right">
                        <a class="active" href="about.jsp">About</a>
                    </div>
                </div>
            </div>
            <div align="center">
                <br>
                <center><h2> Login </h2></center>
                <form action="validateUser" method="post"> 
                <center><div>${error}</div></center>
                <table style="margin:auto;">
                <tr><td>Email Id:</td><td><input type="email" name="emailId" required/></td></tr>
                <tr><td>password:</td><td><input type="password" minlength="6" name="password" required/></td></tr>
                </table>
                <center>
                    <button type="submit">Login</button>
                    </form>
                </center>
                <center>
                     <p> Doesn't have an account?. Create one</p>
                    <button onclick="location.href='newUser'">Sign up</button>
                </center>
            </div>
        </div>
    </body>
</html> 
