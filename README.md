Project Title

    Howzat is a project that allows user to create,view,edit,delete players,Team,Matches.
    This has login, add players to team, add teams to match, and some useful features.
    
Prerequisites
    
    ##You would need to install the following to run this code
        * JAVA 8
        * Mysql
        * Apache-tomcat 8.5.43
        * Apache-maven

Installing

    * Install java in your system.
    * 

Technologies Used

    * Front-End: Jsp
    * Back-End: Java
    * Database Used: Mysql
    * FrameWorks Used: Hibernate, spring
    
Build With

    * Maven - Dependency Management

