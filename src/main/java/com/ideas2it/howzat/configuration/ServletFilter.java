package com.ideas2it.howzat.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;

public class ServletFilter implements Filter{
   
    @Override
    public void init(FilterConfig arg0) {
    }

    @Override   
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) {
        try {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            boolean isLoggedIn = (null != session
                                    && null != session.getAttribute("emailId"));
            String loginURI = httpRequest.getContextPath() + "/validateUser";
            String signinURI = httpRequest.getContextPath() + "/newUser";
            String createURI = httpRequest.getContextPath() + "/createUser";
            boolean isSigninRequest = 
                    httpRequest.getRequestURI().equals(signinURI);
            boolean isLoginRequest = 
                    httpRequest.getRequestURI().equals(loginURI);
            boolean isCreateRequest = 
                    httpRequest.getRequestURI().equals(createURI);
            if (isLoggedIn || isLoginRequest 
                    || isSigninRequest || isCreateRequest) {
                chain.doFilter(request, response);
            } else {
                RequestDispatcher dispatcher =
                        request.getRequestDispatcher("index.jsp");
                dispatcher.forward(request, response);
            }
        } catch (Throwable e){
            e.printStackTrace();
        }
    }
    
    @Override
    public void destroy() {
    }
}  
