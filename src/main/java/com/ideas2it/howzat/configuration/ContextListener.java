package com.ideas2it.howzat.configuration;

import java.io.File;
 
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
 
import org.apache.log4j.PropertyConfigurator;
 
public class ContextListener implements ServletContextListener {
 
    /**
     * Notification that the web application initialization process is starting.
     * All ServletContextListeners are notified of context initialization before
     * any filter or servlet in the web application is initialized.
     */
    public void contextInitialized(ServletContextEvent event) {     
        PropertyConfigurator.configure("log4j.properties");
         
    }
    
    /**
     * Notification that the servlet context is about to be shut down. 
     * All servlets and filters have been destroyed before 
     * any ServletContextListeners are notified of context destruction.
     */ 
    public void contextDestroyed(ServletContextEvent event) {

    }  
}
