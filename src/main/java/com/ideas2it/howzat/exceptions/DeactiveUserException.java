package com.ideas2it.howzat.exceptions;

/**
 * User-defined exception used to throw when user account  is deactivated 
 * 
 */
public class DeactiveUserException extends RuntimeException {
    
    /**
     * DeactiveUserException used to throw when user account  is deactivated
     * 
     * @param String sentence - the message to be written in exception.
     */
    public DeactiveUserException(String sentence) {
        super(sentence);
    }
}
