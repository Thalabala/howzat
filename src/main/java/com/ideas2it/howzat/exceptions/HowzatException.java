package com.ideas2it.howzat.exceptions;

/**
 * User-defined exception used to throw when hibernate exception occured in DAO
 * 
 */
public class HowzatException extends Exception {
    
    /**
     * HowzatException used to throw when hibernate exception occured in DAO
     * 
     * @param String sentence - the message to be written in exception
     * @param Throwable exception - exception caught in DAO 
     */
    public HowzatException(String sentence, Throwable exception) {
        super(sentence, exception); 
    }
}

