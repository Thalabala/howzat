package com.ideas2it.howzat.exceptions;

/**
 * User-defined exception used to throw when user is invalid  or not found in db 
 * 
 */
public class InvalidUserException extends RuntimeException {
    
    /**
     * InvalidUserException used to throw when user is invalid  
     * or not found in db 
     * 
     * @param String sentence - the message to be written in exception
     */
    public InvalidUserException(String sentence) {
        super(sentence);
    }
}
