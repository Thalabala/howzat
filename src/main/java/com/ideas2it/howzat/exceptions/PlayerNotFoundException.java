package com.ideas2it.howzat.exceptions;

/**
 * User-defined exception used to throw when desired playerId not found in db
 * 
 */
public class PlayerNotFoundException extends RuntimeException {
    
    /**
     * PlayerNotFoundException used to throw when desired playerId not found in db 
     * 
     * @param String sentence - the message to be written in exception
     */
    public PlayerNotFoundException(String sentence) {
        super(sentence);
    }
}
