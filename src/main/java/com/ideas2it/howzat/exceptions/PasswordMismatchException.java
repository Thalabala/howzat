package com.ideas2it.howzat.exceptions;

/**
 * User-defined exception used to throw when user entered password and 
 * saved password mismatches
 * 
 */
public class PasswordMismatchException extends RuntimeException {
    
    /**
     * PasswordMismatchException used to throw when user entered password and 
     * saved password mismatches 
     * 
     * @param String sentence - the message to be written in exception
     */
    public PasswordMismatchException(String sentence) {
        super(sentence);
    }
}
