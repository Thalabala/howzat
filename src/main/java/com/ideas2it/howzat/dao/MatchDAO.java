package com.ideas2it.howzat.dao;

import java.lang.Boolean;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Team;

import org.hibernate.HibernateException;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for match model done here
 * It allows to insert, fetch, update, remove data from db
 */
public interface MatchDAO {
       
    /**
     * Gets match object, saves the object 
     * 
     * @param match - match object containing fields
     * @return Match - match object created by db
     */
    public Match createMatch(Match match) throws HowzatException;

    /**
     * Gets the match id  , retrives match object
     *
     * @param matchId - match id to be retrived 
     * @ returns Match - match object
     */
    public Match viewMatch(int matchId) throws HowzatException;
 
    /**
     * Fetches all match objects 
     * 
     * @returns List<Match> matches - list of all match objects 
     */
    public List<Match> viewallMatches() throws HowzatException;

    /**
     * Updated match informations will be updated into table
     * 
     * @param match - object containing updated info
     */
    public void updateMatch(Match match) throws HowzatException;

    /**
     * Gets object , delets it from db
     * 
     * @param match -  object to be deleted
     */    
    public void deleteMatch(Match match) throws HowzatException;
}
