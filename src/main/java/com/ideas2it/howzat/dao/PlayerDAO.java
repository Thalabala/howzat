package com.ideas2it.howzat.dao;

import java.lang.Boolean;
import java.util.List;
import java.util.ArrayList;
 
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Contact;
import com.ideas2it.howzat.exceptions.HowzatException;

import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;  

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for player model done here
 * It allows to insert, fetch, update, remove data from db
 */
public interface PlayerDAO {
    
    /**
     * Counts and returns total rows count in player table
     *
     * @returns size - size of table
     */    
    public int totalIdInTable() throws HowzatException;
    
    /**
     * Inserts player informations present in object 
     * into respective columns in table 
     *
     * @param Player - player object with player data
     * @returns player - created player object with id
     */    
    public Player insertPlayer(Player player) throws HowzatException;
        
    /**
     * Fetches the player object using playerId
     *
     * @param playerId - player id to retrived
     * @returns Player - corresponding player object to id
     */
    public Player fetchPlayerById(int playerId) throws HowzatException;

    /**
     * Gets the player object index 
     *
     * @ returns players - list of object objects
     */    
    public List<Player> fetchPlayers(int offSet) throws HowzatException;

    /**
     * Updated information will be updated into table
     * 
     * @param Player - player object containing updated info
     */    
    public Player updatePlayer(Player player) throws HowzatException;  
    
    /**
     * Fetches players corresponding to country and 
     * not assigned to any other team
     *
     * @returns players - List of player objects
     */    
    public List<Player> fetchCountryPlayers(String country) 
            throws HowzatException;
    
    /**
     * Fetches players corresponding to player id's using in query
     *
     * @returns players - List of player
     */
    public List<Player> fetchPlayersFromId(List<Integer> playerIds) 
            throws HowzatException;
            
    public List<Player> fetchPlayersFromTeamId(int teamId) 
            throws HowzatException;
    
}

