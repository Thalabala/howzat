package com.ideas2it.howzat.dao.impl;

import java.lang.Boolean;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.NoResultException;
 
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.Over;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.dao.OverDAO;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for player model done here
 * It allows to insert, fetch, update, remove data from db
 */
@Repository
public class OverDAOImpl implements OverDAO {
    
    private SessionFactory factory;
    
    private static final Logger logger = Logger.getLogger(OverDAO.class);
    
    @Autowired
    public OverDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }    
    
    @Override    
    public int insertOver(Over over) throws HowzatException {
        int overId = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            overId = (Integer) session.save(over);
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_CREATING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {      
            if(null != session && null != transaction) {
               transaction.commit();      
               session.close();
            }        
        }
        return overId;
    }
        
    @Override
    public Over fetchOver(int overId) throws HowzatException { 
        Over over = null;
        Session session = null;
        try {
            session = factory.openSession();  
            over = (Over) session.get(Over.class, overId);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
            }        
        }
        return over;
    }

    @Override    
    public void updateOver(Over over) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.update(over);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_UPDATING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }        
        }
    } 
    
    @Override
    public int getLastOverNoByMatchId(int matchId) 
            throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        int overNo = 0;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            Query query = session.createQuery("select max(overNo) from Over where "
                              +"match_id = :matchId");
            query.setParameter("matchId",matchId);
            if(null != query.getSingleResult()) {
                overNo = (Integer) query.getSingleResult();
            } else {
                logger.error("cdfshjnftgjh");
            }
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } catch (NoResultException e) {
            overNo = 0;
        } finally {
            if(null != session) {
                transaction.commit();
                session.close();
            }
        }
        return overNo;
    } 
}

