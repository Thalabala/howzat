package com.ideas2it.howzat.dao.impl;

import java.lang.Boolean;
import java.util.List;
import java.util.ArrayList;
 
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Contact;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.dao.PlayerDAO;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;  

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for player model done here
 * It allows to insert, fetch, update, remove data from db
 */
@Repository
public class PlayerDAOImpl implements PlayerDAO {
    
    private SessionFactory factory;
    
    private static final Logger logger = Logger.getLogger(PlayerDAO.class);
    
    @Autowired
    public PlayerDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }
    
    @Override    
    public int totalIdInTable() throws HowzatException {
        int size = 0;
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("select count(id) from Player"
                    +" where status = 1");
            size = ((Number)query.uniqueResult()).intValue();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_VALIDATING_PLAYER +e.getMessage());
            throw new HowzatException(Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
            }
            return size;
        }
    }    
    
    @Override    
    public Player insertPlayer(Player player) throws HowzatException {
        int playerId = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            playerId = (Integer) session.save(player);
        } catch (HibernateException e){
            e.printStackTrace();
            logger.error(Constants.ERROR_AT_CREATING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {      
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
                player.setId(playerId);
            }        
        }
        return player;
    }
        
    @Override
    public Player fetchPlayerById(int playerId) throws HowzatException { 
        Player player = null;
        Session session = null;
        Player playerWithTeam = new Player(); 
        try {
            session = factory.openSession();  
            player = (Player) session.get(Player.class, playerId);
            playerWithTeam.setTeam(player.getTeam());
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
                player.setTeam(playerWithTeam.getTeam());
            }        
        }
        return player;
    }

    @Override   
    public List<Player> fetchPlayers(int offSet) throws HowzatException { 
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("from Player where status = 1");
            query.setFirstResult(offSet);
            query.setMaxResults(Constants.NO_OF_RECORDS_PER_PAGE);
            players = query.list();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {  
            if(null != session) {
                session.close();
            }
        }
        return players;
    }  

    @Override    
    public Player updatePlayer(Player player) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.update(player);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_UPDATING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }        
        }
        return player;
    }
    
    @Override    
    public List<Player> fetchCountryPlayers(String country) 
            throws HowzatException { 
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("from Player where country = "
                    +" :country and team_Id is null",Player.class);
            query.setString("country",country);
            players = query.list();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
            }
            return players;
        }
    } 
    
    @Override
    public List<Player> fetchPlayersFromId(List<Integer> playerIds) 
            throws HowzatException { 
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();  
            Query query = session.createQuery("from Player where "
                              +"id in :playerIds",Player.class);
            query.setParameterList("playerIds",playerIds);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
            }
        }
        return players;
    } 
    
    @Override
    public List<Player> fetchPlayersFromTeamId(int teamId) 
            throws HowzatException { 
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();  
            Query query = session.createQuery("from Player where "
                              +"team_id =:teamId",Player.class);
            query.setParameter("teamId",teamId);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_FETCHING_PLAYER 
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session) {
                session.close();
            }
        }
        return players;
    }
}

