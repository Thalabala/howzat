package com.ideas2it.howzat.dao.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.dao.MatchDAO;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

@Repository
public class MatchDAOImpl implements MatchDAO {

    private SessionFactory factory;

    private static final Logger logger = Logger.getLogger(MatchDAO.class);
    
    @Autowired
    public MatchDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }
       
    @Override
    public Match createMatch(Match match) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try { 
            session = factory.openSession();  
            transaction = session.beginTransaction();
            int matchId = (Integer) session.save(match);
            match.setId(matchId);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_CREATING_MATCH 
                    +e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }
            return match;  
        }
    }

    @Override
    public Match viewMatch(int matchId) throws HowzatException {
        Session session = null;
        Match match = null;
        try {
            session = factory.openSession();
            match = session.get(Match.class,matchId);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_MATCH  
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {              
            if(null != session) {
                session.close();
            }
            return match;
        }  
    }
 
    @Override
    public List<Match> viewallMatches() throws HowzatException {
        List<Match> matches = null;
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("from Match", Match.class);
            matches = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_FETCHING_MATCH  +e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM, e);
        } finally {
            if(null != session) {
                session.close();
            }
        }
        return matches;
    }

    @Override
    public void updateMatch(Match match) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try{
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.update(match);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_UPDATING_MATCH  
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }  
        }
    }    

    @Override    
    public void deleteMatch(Match match) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.delete(match);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_DELETING_MATCH  
                    +e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally { 
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            } 
        } 
    } 
}
