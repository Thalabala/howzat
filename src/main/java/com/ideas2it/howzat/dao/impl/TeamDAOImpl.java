package com.ideas2it.howzat.dao.impl;

import java.lang.Boolean;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.dao.TeamDAO;

import org.apache.log4j.Logger;
import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.query.Query;

@Repository
public class TeamDAOImpl implements TeamDAO {

    private SessionFactory factory;
    
    private static final Logger logger = Logger.getLogger(TeamDAO.class);
    @Autowired
    public TeamDAOImpl(SessionFactory factory){
        this.factory = factory;
    }
    
    @Override
    public int totalIdInTable() throws HowzatException {
        int size = 0;
        Session session = null; 
        try {
            session = factory.openSession();  
            Query query = session.createQuery("select count(id) from team");
            size = ((Number)query.uniqueResult()).intValue();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_TABLE_SIZE
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally { 
            if (null != session) {
                session.close();
            }
            return size;
        }
    }   
    
    @Override
    public Team createTeam(Team team) throws HowzatException {
        int teamId = 0;
        Session session = null;
        Transaction transaction = null;
        try {    
            session = factory.openSession();  
            transaction = session.beginTransaction();
            teamId = (Integer) session.save(team);
            System.out.println(teamId);
            team.setId(teamId);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_CREATING_TEAM
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }             
            return team; 
        }
    }

    @Override  
    public Team viewTeam(int teamId) throws HowzatException {
        Set<Player> players = new HashSet<Player>();
        Session session = null;
        Team team = null;
        try {
            session = factory.openSession();
            team = session.get(Team.class,teamId);
            System.out.println(team);
            players.addAll(team.getTeamPlayers());
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_TEAM
                    + teamId + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {    
            if (null != session) {
                session.close();
            }
            team.setTeamPlayers(players);   
            System.out.println(team);
            return team;   
        }
    }
    
    @Override  
    public List<Team> viewTeams(List<Integer> teamIds) throws HowzatException {
        List<Team> teams = new ArrayList();
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("from Team where "
                              +"id in :teamIds",Team.class);
            query.setParameterList("teamIds",teamIds);
            teams = query.list();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_TEAM
                    + teamIds + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {   
            if (null != session) {
                session.close();
            }   
            return teams;   
        }
    }

    @Override 
    public Team viewTeamWithMatch(int teamId) throws HowzatException {
        Set<Match> teammatches = new HashSet<Match>();
        Session session = null;
        Team team = null;
        try {
            session = factory.openSession();
            team = session.get(Team.class,teamId);
            teammatches.addAll(team.getMatches());
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_TEAM
                    + teamId + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {    
            if(null != session) {
                session.close();
            }
            team.setMatches(teammatches);   
            return team;   
        }
    }
 
    @Override
    public List<Team> viewallTeams() throws HowzatException {
        List<Team> teams = new ArrayList<Team>();
        Session session = null;
        try {
            session = factory.openSession();  
            teams = session.createQuery("from Team",Team.class).list();
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_FETCHING_TEAM
                    + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {    
            if(null != session) {
                session.close();
            }
            return teams;
        }
    }

    @Override
    public void updateTeam(Team team) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.update(team);
        } catch (HibernateException e){
            transaction.rollback();
            logger.error(Constants.ERROR_AT_UPDATING_TEAM
                + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {    
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }  
        }
    }    

    @Override  
    public void deleteTeam(Team team) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.delete(team);
        } catch (HibernateException e){
            logger.error(Constants.ERROR_AT_DELETING_TEAM
                + e.getMessage());
            throw new HowzatException (Constants.CONNECTION_PROBLEM,e);
        } finally {
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            }  
        }
    }         
}
