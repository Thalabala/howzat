package com.ideas2it.howzat.dao.impl;

import java.lang.IndexOutOfBoundsException;
import java.lang.Boolean;
import java.util.List;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.dao.UserDAO;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.PersistenceException;

@Repository
public class UserDAOImpl implements UserDAO {
    
    private SessionFactory factory;
    
    private static final Logger logger = Logger.getLogger(UserDAO.class);
    
    @Autowired
    public UserDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }
   
    @Override   
    public void createUser(User user) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.save(user);
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_AT_CREATING_USER + e);
            throw new HowzatException(Constants.ERROR_AT_CREATING_USER, e);
        } catch (PersistenceException e) {
            transaction.rollback();
            logger.error("duplicate emailId " + e);
            throw new HowzatException("email already exist", e);
        } finally {    
            if(null != session  && null != transaction) {
                transaction.commit();      
                session.close();
            } 
        }  
    }
    
    @Override    
    public User fetchUser(String emailId) throws HowzatException {
        User user = null;
        Session session = null;
        try {
            session = factory.openSession(); 
            Query query = session.createQuery("from User where email_id"
                                +" = :emailId");
            query.setParameter(Constants.EMAIL_ID,emailId);
            List users = query.list();
            user = (User) users.get(0);
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_AT_FETCHING_USER + e);
            throw new HowzatException(Constants.ERROR_AT_FETCHING_USER, e);
        } catch (IndexOutOfBoundsException e) {
            logger.error(Constants.INVALID_USER + e);
            return null;
        } finally {    
            if(null != session) {
                session.close();
            }
        }
        return user;
    }
    
    @Override    
    public void updateUser(User user) throws HowzatException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();  
            transaction = session.beginTransaction();
            session.update(user);
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_AT_UPDATING_USER + e);
            throw new HowzatException(Constants.ERROR_AT_UPDATING_USER, e);
        } finally {    
            if(null != session && null != transaction) {
                transaction.commit();      
                session.close();
            } 
        }       
    }
}

