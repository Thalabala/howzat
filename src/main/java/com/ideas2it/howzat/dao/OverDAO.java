package com.ideas2it.howzat.dao;

import java.lang.Boolean;
import java.util.List;
import java.util.ArrayList;
 
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.Over;
import com.ideas2it.howzat.exceptions.HowzatException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;  

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for Over model done here
 * It allows to insert, fetch, update, remove data from db
 */
public interface OverDAO {
    
    /**
     * Inserts over informations present in object 
     * into respective columns in table 
     *
     * @param over - over object with over data
     * @returns over - created over object with id
     */   
    public int insertOver(Over over) throws HowzatException;
    
    /**
     * Fetches the over object using overId
     *
     * @param overId - over id to retrived
     * @returns over - corresponding over object to id
     */
    public Over fetchOver(int overId) throws HowzatException;
    
    /**
     * Updated information will be updated into table
     * 
     * @param over - over object containing updated info
     */
    public void updateOver(Over over) throws HowzatException;
    
    /**
     * Returns last maximum over number from over table
     *
     * @param matchId - id of match from which last over number to be obtained 
     * @returns overNo - last over number from table
     */
    public int getLastOverNoByMatchId(int matchId) throws HowzatException;
}

