package com.ideas2it.howzat.dao;

import java.lang.Boolean;
import java.util.List;

import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.exceptions.HowzatException;

import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.hibernate.query.Query;  

/**
 * Data access layer gives access to database by creating db connection
 * All CRUD operations for user model done here
 * It allows to insert, fetch, update, remove data from db
 */
public interface UserDAO {
   
    /**
     * Inserts the user object in database
     * 
     * @param User - user object with all field data
     */    
    public void createUser(User user) throws HowzatException;
    
    /**
     * Fetches the user from database 
     * 
     * @param emailId - user emailId 
     */    
    public User fetchUser(String emailId) throws HowzatException;
    
    /**
     * updates the fields in database
     * 
     * @param User - user object with all field data
     */    
    public void updateUser(User user) throws HowzatException;
}

