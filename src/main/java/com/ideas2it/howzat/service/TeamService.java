package com.ideas2it.howzat.service;

import java.lang.String;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.io.PrintWriter;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.dao.TeamDAO;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.TeamInfo;
import com.ideas2it.howzat.util.CommonUtil;

/**
 * The class which contains all business logics and calculations,
 * process done for user informations.
 *
 */
public interface TeamService {
    
    /**
     * Get list of string contains player basic info country filtered 
     * accessing playerDAo through playerservice 
     *   
     * @param String country - by which players to be filtered
     * @returns List<String> - contains player basic info 
     */
    public List<Player> fetchCountryPlayer(String country) throws HowzatException;  
        
    /**
     * Fetches all teams from db,
     *   
     * @returns List<Team> - list of all teams 
     */   
    public List<Team> fetchAllTeams() throws HowzatException;
    
    public List<TeamInfo> fetchAllTeamsAsInfo() throws HowzatException;
    
    /**
     * Team field inputs are setted into object
     * @param playerFields - string array containing user inputs
     * 
     * @return integer created player id 
     */
    public TeamInfo createTeam(TeamInfo teamInfo) throws HowzatException ;
    
    
    /** 
     * Gets update input , set updated team to DAO  
     *  
     * @param teanID - Team to be updated
     * @param integer option - to identify which field to be updated 
     */
    public void updateTeam(int teamId , String[] playersIdToAdd, 
            String[] playersIdToRemove, String teamName) 
            throws HowzatException;

    
    /** 
     * Returns team object with respect to teamId  
     *  
     * @param teamID - Id of team to be fetched
     * @return Team object - respective team object to team id  
     */
    public Team fetchTeam(int teamId) throws HowzatException;

    /** 
     * Delete team object with respect to teamId  
     *  
     * @param teamID - Id of team to be deleted 
     */
    public void deleteTeam(int teamId) throws HowzatException;    
    
    /**
     * Fetches team with matches assigned to it
     *
     *@return Team - corresponding team
     */
    public Team viewTeamWithMatch(int teamId) throws HowzatException;
    
    /**
     * Fetches all teams corresponding to teamids
     *
     *@return List<Team> - corresponding teams
     */
    public List<Team> viewTeams(String[] teamIds) throws HowzatException;
    
    /**
     * Fetches team and country players, sets to info object and returns
     *
     *@return TeamInfo - object containing team and players
     */
    public TeamInfo fetchTeamInfo(int teamId, boolean view) throws HowzatException;
    
    /**
     * Team validation checks the minimal conditions of player types in team
     * Fetches count of each player type and validate them
     * 
     * @param teamID - team to be validated  
     * @return int[] - returns count of players of each player role 
     */
    public int[] validateTeam(Team team) throws HowzatException;
}
