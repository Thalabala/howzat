package com.ideas2it.howzat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.OverDetailInfo;
import com.ideas2it.howzat.info.StartMatchInfo;
import com.ideas2it.howzat.service.MatchService;
import com.ideas2it.howzat.service.OverDetailService;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.util.CommonUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author J.Balakumaran
 * created on 24 aug 2019.
 */
public interface OverDetailService {

    public JSONObject getPlayersToStart(int batsmanId,
             int bowlerId,int matchId,int overId) throws HowzatException;
}
