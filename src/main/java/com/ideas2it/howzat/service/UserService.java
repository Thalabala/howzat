package com.ideas2it.howzat.service;

import java.util.List;

import com.ideas2it.howzat.dao.UserDAO;
import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.exceptions.DeactiveUserException;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.InvalidUserException;
import com.ideas2it.howzat.exceptions.PasswordMismatchException;
import com.ideas2it.howzat.info.UserInfo;

/**
 * The class which contains all business logics and calculations,
 * process done for user informations.
 *
 */
public interface UserService {
    
    /**
     * Gets input for required fields of user,
     * 
     * @param String name - contains user name
     * @param String emailId - contains user email-id
     * @param String role - contains user role
     * @param String gender - contains user gender
     * @param String contactNumber - contains user contact number
     * @param String password - contains user password
     */
    public void createUser(UserInfo userInfo) 
                    throws HowzatException;
    
    /**
     * Fetches the user object respective to given emailId
     *
     * @param String emailId - user emailId of user to be fetched
     * @return User - user object respective to emailId
     */
    public User fetchUser(String emailId) throws HowzatException;
    
    /**
     * Update the user object 
     *
     * @param User user - user object to be updated
     */
    public void updateUser(User user) throws HowzatException;
    
    /**
     * Authendicate the user by comparing passwords 
     *
     * @param User user - user object to be updated
     */
    public boolean authendicateUser(String emailId, String encryptPassword) 
            throws PasswordMismatchException,
            DeactiveUserException, HowzatException;
}
