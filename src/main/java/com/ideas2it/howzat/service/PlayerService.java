package com.ideas2it.howzat.service;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.Part;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.howzat.dao.PlayerDAO;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Contact;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.PlayerPaginationInfo;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.AgeUtil;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.PlayerNotFoundException;

/**
 * The class which contains all business logics and calculations,
 * process done for player informations.
 *
 */
public interface PlayerService {
  
    /**
     * Validates player whether present or not 
     *
     * @param playerId - string array containing user inputs
     * @return integer created player id 
     */
    Player isValidId(int playerId) 
            throws HowzatException,PlayerNotFoundException;
    
    /**
     * Returns number of rows in 
     *
     * @param playerFields - string array containing user inputs
     * @return integer created player id 
     */
    int totalIdInTable() throws HowzatException;
    
    /**
     * player field inputs are setted into object
     *
     * @param playerFields - string array containing user inputs
     * @return integer created player id 
     */
    PlayerInfo createPlayer(CommonsMultipartFile file, PlayerInfo playerInfo) 
            throws HowzatException, IOException; 
    
    /**
     * fetch corresponding object for the id
     *
     * @param int id - id of player
     * @return player object 
     */  
    Player fetchPlayerById(int id) throws HowzatException;
    
   /**
     * Sets picture 
     *
     * @param Part filePart - profile picture in part object
     * @param int playerId - player id whode pic to be uploaded 
     * @throws HowzatException - exception from DAO is passed to controller
     */  
    Player uploadPictureURL(CommonsMultipartFile file, Player player) 
            throws HowzatException, IOException;
    
    
    PlayerInfo fetchPlayerInfo(int id) throws HowzatException;
    
    /** 
     * gets update input , updates the player object  
     *  
     * @param player object - player to be updated
     * @param string userInput - validated userinput which gonna be saved
     * @param integer option - to identify which field to be updated 
     */
    PlayerInfo updatePlayer(CommonsMultipartFile file, PlayerInfo playerInfo) 
            throws HowzatException, IOException;
    
    
    /**
     * Fetch players using offset and limit 
     *
     * @param offset - from which starting point it should retrive
     * @return List<Player> - list of players
     */
    PlayerPaginationInfo fetchAllPlayer(int offset) throws HowzatException;
    
    /**
     * delete player from db 
     *
     * @param object player - player to be deleted 
     */
    void deletePlayer(int playerId) throws HowzatException;

    /**
     * Fetches players from corresponding country 
     *
     * @param object player - player to be deleted 
     */
    List<Player> fetchCountryPlayer(String country) 
            throws HowzatException;
   
    /**
     * Fetches players from string of players
     *
     * @param object player - player to be deleted 
     */
    List<Player> fetchPlayersFromId(String[] playerIds) 
            throws HowzatException;
    
    /**
     * Fetches players 
     *
     * @param int page - page number 
     */        
    public JSONArray fetchPlayersByPage(int page) throws HowzatException;
    
    public List<Player> fetchPlayersFromTeamId(int teamId) 
            throws HowzatException;
    
}

