package com.ideas2it.howzat.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.dao.TeamDAO;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.TeamInfo;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.service.TeamService;
import com.ideas2it.howzat.util.CommonUtil;

@Service
public class TeamServiceImpl implements TeamService {
   
    private TeamDAO teamDAO;
    private PlayerService playerService;
    
    @Autowired
    public TeamServiceImpl(TeamDAO teamDAO, PlayerService playerService){
        this.teamDAO = teamDAO;
        this.playerService = playerService;
    }
    
    @Override
    public TeamInfo createTeam(TeamInfo teamInfo) throws HowzatException {
        Team team = CommonUtil.convertTeamInfoToTeamEntity(teamInfo);
        team = teamDAO.createTeam(team);
        String teamCountry = team.getCountry().toString();
        List<Player> players = fetchCountryPlayer(teamCountry);
        List<PlayerInfo> playersInfo = CommonUtil.convertPlayersToPlayersInfo(
                                            players);
        teamInfo = CommonUtil.convertTeamEntityToTeamInfo(team);
        teamInfo.setPlayers(playersInfo);
        return teamInfo;
    }
    
    @Override
    public void updateTeam(int teamId , String[] playersIdToAdd, 
            String[] playersIdToRemove, String teamName) 
            throws HowzatException {
        Team team = fetchTeam(teamId);
        if (null != teamName) {
            team.setTeamName(teamName);
        }
        if ((null != playersIdToAdd) && (0 < playersIdToAdd.length)){
            List<Player> selectedPlayers = playerService.fetchPlayersFromId(
                                            playersIdToAdd);
            Set<Player> teamPlayers = team.getTeamPlayers();
            teamPlayers.addAll(new HashSet(selectedPlayers));
            team.setTeamPlayers(teamPlayers);
        }
        if ((null != playersIdToRemove) && (0 < playersIdToRemove.length)){
            List<Player> selectedPlayers = playerService.fetchPlayersFromId(
                                                playersIdToRemove);
            Set<Player> teamPlayers = team.getTeamPlayers();
            for(Player player : selectedPlayers) {
                teamPlayers.remove(player);
            }
            team.setTeamPlayers(teamPlayers);
        }
        teamDAO.updateTeam(team);
    }
    
    @Override
    public Team fetchTeam(int teamId) throws HowzatException {
        return teamDAO.viewTeam(teamId);
    }
    
    public TeamInfo fetchTeamInfo(int teamId, boolean view) throws HowzatException {
        Team team = teamDAO.viewTeam(teamId);
        String teamCountry = team.getCountry().toString();
        TeamInfo teamInfo = CommonUtil.convertTeamEntityToTeamInfo(team);
        List<Player> teamPlayers = new ArrayList(team.getTeamPlayers());   
        List<PlayerInfo> teamPlayersAsInfo = 
                        CommonUtil.convertPlayersToPlayersInfo(teamPlayers);
        teamInfo.setTeamPlayers(new HashSet(teamPlayersAsInfo));
        if(!view){
            List<Player> nonTeamplayers = fetchCountryPlayer(teamCountry);
            List<PlayerInfo> players = CommonUtil.convertPlayersToPlayersInfo(
                                            nonTeamplayers);
            teamInfo.setPlayers(players);
        }         
        int[] playerCounts = validateTeam(team);
        teamInfo.setPlayerCounts(playerCounts);
        return teamInfo;
    }
    
    @Override
    public void deleteTeam(int teamId) throws HowzatException {
        Team team = fetchTeam(teamId);
        teamDAO.deleteTeam(team);    
    }     
    
    @Override
    public Team viewTeamWithMatch(int teamId) throws HowzatException {
        return teamDAO.viewTeamWithMatch(teamId);
    }
    
    @Override
    public List<Team> viewTeams(String[] teamIds) throws HowzatException {
        List<Integer> teamIdsToBeFetched = new ArrayList<Integer>();
        for(String teamId : teamIds) {
            teamIdsToBeFetched.add(Integer.parseInt(teamId));
        }
        return teamDAO.viewTeams(teamIdsToBeFetched);
    }
    
    @Override
    public List<Player> fetchCountryPlayer(String country) throws HowzatException {
        return playerService.fetchCountryPlayer(country);
    }  
    
    public List<Team> fetchTeams(List<Integer> teamsIdsToFetched) 
            throws HowzatException {
        return teamDAO.viewTeams(teamsIdsToFetched);
    }
        
    @Override    
    public List<TeamInfo> fetchAllTeamsAsInfo() throws HowzatException {
        List<Team> teams = teamDAO.viewallTeams();
        List<TeamInfo> allTeams = CommonUtil.convertAllTeamsToTeamsInfo(teams);
        return allTeams;
    }
        
    @Override  
    public List<Team> fetchAllTeams() throws HowzatException {
        return teamDAO.viewallTeams();
    }
    
    @Override
    public int[] validateTeam(Team team) throws HowzatException {
        Set<Player> teamPlayers = team.getTeamPlayers();
        int batsmanCount = 0;
        int bowlerCount = 0;
        int wicketkeeperCount = 0;
        int allRounderCount = 0;
        String playerRole = null;
        for (Player player : teamPlayers) {
            playerRole = player.getPlayerType();
            switch (playerRole) {
                case "Batsman" :
                    batsmanCount++;
                    break;
                case "Bowler" : 
                    bowlerCount++;
                    break;
                case "WicketKeeper" :
                    wicketkeeperCount++;
                    break;
                case "All-Rounder" :
                    allRounderCount++;
                    break;
            }
        } 
        int[] count = {batsmanCount, bowlerCount, 
                        wicketkeeperCount, allRounderCount};
        int teamPlayersCount = batsmanCount + bowlerCount + wicketkeeperCount 
                                + allRounderCount;
        if(11 == teamPlayersCount) {
            if((3 <= batsmanCount) && (3 <= bowlerCount) &&
                            (1 == wicketkeeperCount)) {
                team.setStatus(Boolean.TRUE);    
            }
        } else {
            team.setStatus(Boolean.FALSE);
        }
        teamDAO.updateTeam(team);
        return count;   
    }
    
}
