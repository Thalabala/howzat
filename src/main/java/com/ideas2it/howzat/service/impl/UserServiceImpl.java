package com.ideas2it.howzat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.howzat.dao.UserDAO;
import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.exceptions.DeactiveUserException;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.InvalidUserException;
import com.ideas2it.howzat.exceptions.PasswordMismatchException;
import com.ideas2it.howzat.info.UserInfo;
import com.ideas2it.howzat.service.UserService;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.EncryptionUtil;

/**
 * The class which contains all business logics and calculations,
 * process done for user informations.
 *
 */
@Service
public class UserServiceImpl implements UserService {
    
    private UserDAO userDAO;
    
    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
        
    @Override
    public void createUser(UserInfo userInfo) 
                    throws HowzatException {
        User user = CommonUtil.convertUserInfoToUserEntity(userInfo);
        String encryptedPassword = EncryptionUtil.encryptPassword(
                                        user.getPassword());
        user.setPassword(encryptedPassword);                       
        userDAO.createUser(user);
    } 
    
    @Override
    public User fetchUser(String emailId) 
            throws HowzatException, InvalidUserException {
        User user = userDAO.fetchUser(emailId);
        if (null != user) {
            return user;
        } else {
            throw new InvalidUserException("Invalid user");
        }
    }
    
    @Override
    public void updateUser(User user) throws HowzatException {
        userDAO.updateUser(user);
    }
    
    @Override
    public boolean authendicateUser(String emailId, String encryptPassword) 
            throws PasswordMismatchException, 
            DeactiveUserException, HowzatException {
        User user = fetchUser(emailId);
        int wrongAttempts = user.getAttempts();
        if(user.getPassword().equals(encryptPassword) && user.getStatus()) {
            return Boolean.TRUE;
        } else if (3 >= wrongAttempts) {
            user.setAttempts(wrongAttempts+ 1);
            updateUser(user);
            throw new PasswordMismatchException("Invalid Password "
                    +",Remaining Attempts : " + (3 - user.getAttempts()));
        } else {
            user.setStatus(Boolean.FALSE);
            updateUser(user);
            throw new DeactiveUserException("User Account Deactivated ");
        }
    }
}
