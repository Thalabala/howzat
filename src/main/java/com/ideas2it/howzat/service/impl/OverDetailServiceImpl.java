package com.ideas2it.howzat.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List; 

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.DeliveryResult;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.entities.OverDetail;
import com.ideas2it.howzat.info.OverDetailInfo;
import com.ideas2it.howzat.service.OverService;
import com.ideas2it.howzat.service.MatchService;
import com.ideas2it.howzat.service.OverDetailService;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.util.CommonUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author J.Balakumaran
 * created on 24 aug 2019.
 */
@Service
public class OverDetailServiceImpl implements OverDetailService {

    private PlayerService playerService;
    private MatchService matchService;
    private OverService overService;

    @Autowired
    public OverDetailServiceImpl(PlayerService playerService, 
            MatchService matchService, OverService overService) {
        this.playerService = playerService;
        this.matchService = matchService;
        this.overService = overService;
    }

    @Override
    public JSONObject getPlayersToStart(int batsmanId,
             int bowlerId,int matchId,int overId) throws HowzatException {
        OverDetailInfo overDetailInfo = new OverDetailInfo();
        OverDetail overDetail = new OverDetail();
        int generetedRuns = CommonUtil.fetchPrioritizedRandomRuns(33);
        if((0 <= generetedRuns) && (6 >= generetedRuns)) {
            overDetail.setRuns(generetedRuns);        
        } else if(11 == generetedRuns) {
            overDetail.setRuns(0);
        } else if((11 > generetedRuns) && (6 < generetedRuns)) {
            overDetail.setRuns(1);
        }
        overDetail.setDeliveryResult(DeliveryResult.values()[generetedRuns]);
        Player batsman = playerService.fetchPlayerById(batsmanId);
        overDetail.setBatsman(batsman);
        Player bowler = playerService.fetchPlayerById(bowlerId);
        overDetail.setBowler(bowler);
        Match match = matchService.fetchMatch(matchId);
        overDetail.setMatch(match);
        overService.updateOverwithOverDetail(overDetail, overId, generetedRuns);
        JSONObject scoreCard = new JSONObject();
        if(7 > overDetail.getBallNo()) {
            scoreCard.put("runs", overDetail.getRuns());
            scoreCard.put("deliveryResult", overDetail.getDeliveryResult());
            scoreCard.put("ballNo", overDetail.getBallNo());
        }
        return scoreCard;
    }
    
}
