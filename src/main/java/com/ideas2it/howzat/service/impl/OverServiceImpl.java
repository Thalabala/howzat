package com.ideas2it.howzat.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.NoResultException;

import com.ideas2it.howzat.dao.OverDAO;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Over;
import com.ideas2it.howzat.entities.OverDetail;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.StartMatchInfo;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.service.MatchService;
import com.ideas2it.howzat.service.OverService;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.service.TeamService;
import com.ideas2it.howzat.util.CommonUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author J.Balakumaran
 * created on 24 aug 2019.
 */
@Service
public class OverServiceImpl implements OverService {

    private PlayerService playerService;
    private MatchService matchService;
    private OverDAO overDAO;

    @Autowired
    public OverServiceImpl(PlayerService playerService, 
            MatchService matchService, OverDAO overDAO) {
        this.playerService = playerService;
        this.matchService = matchService;
        this.overDAO = overDAO;
    }

    @Override
    public StartMatchInfo getPlayersToStart(String[] batsmanIds, 
            String[] bowlerId, int matchId) throws HowzatException {
        Over over = new Over();
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        System.out.println("Batsman 2 Id : "+batsmanIds[1]);
        StartMatchInfo startMatchInfo = new StartMatchInfo();
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Match match = matchService.fetchMatch(matchId);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        int overno = overDAO.getLastOverNoByMatchId(matchId);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        over.setOverNo(overno + 1);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        over.setMatch(match);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        int overId = overDAO.insertOver(over);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Over createdOver = overDAO.fetchOver(overId);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        List<Player> batsmans = playerService.fetchPlayersFromId(batsmanIds);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Player bowler = playerService.fetchPlayerById(Integer.parseInt(bowlerId[0]));
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Player firstBatsman = batsmans.get(0);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Player secondBatsman = batsmans.get(1);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setFirstBatsmanId(firstBatsman.getId());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setFirstBatsmanName(firstBatsman.getName());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setSecondBatsmanId(secondBatsman.getId());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setSecondBatsmanName(secondBatsman.getName());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setBowlerId(bowler.getId());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setBowlerName(bowler.getName());
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setMatchId(matchId);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        startMatchInfo.setOverId(overId);
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Team battingTeam = firstBatsman.getTeam();
        System.out.println("batsman 1 Id : "+batsmanIds[0]);
        Team bowlingTeam = bowler.getTeam();
        System.out.println("batting teamId : "+battingTeam.getId());
        System.out.println("Bowling teamId : "+bowlingTeam.getId());
        startMatchInfo.setBattingTeamId(battingTeam.getId());
        startMatchInfo.setBowlingTeamId(bowlingTeam.getId());
        return startMatchInfo;
    }
    
    @Override
    public void updateOverwithOverDetail(OverDetail overDetail, 
            int overId, int runs) throws HowzatException {
        Over over = overDAO.fetchOver(overId);
        Set<OverDetail> overDetails = over.getOverDetails();
        int ballNo = overDetails.size() + 1;
        if(7 > ballNo) {
            over = setBallScoreToOver(over, runs);
            overDetail.setOver(over);
            overDetail.setBallNo(ballNo);
            overDetails.add(overDetail);
            over.setOverDetails(overDetails);
            overDAO.updateOver(over);
        }
    }
    
    
    private Over setBallScoreToOver(Over over, int runs) {
        int totalRuns = over.getRuns();
        if((0 <= runs) && (6 >= runs)) {
            over.setRuns(totalRuns + runs); 
            if(4 == runs) {
                over.setBoundariesCount((over.getBoundariesCount()) + 1);
            } else if(6 == runs) {
                over.setSixerCount((over.getSixerCount()) + 1);
            }
        } else if(11 == runs) {
            over.setRuns(totalRuns);
            over.setWicketCount((over.getWicketCount()) + 1);
        } else if((11 > runs) && (6 < runs)) {
            over.setRuns(totalRuns + 1);
            switch(runs) {
                case 7:
                    over.setWidesCount((over.getWidesCount()) + 1);
                    break;
                case 8:
                    over.setNoBallsCount((over.getNoBallsCount()) + 1);
                    break;
                case 9:
                    over.setByesCount((over.getByesCount()) + 1);
                    break;
                case 10:
                    over.setByesCount((over.getByesCount()) + 1);
                    break;
            }
        }
        return over;
    }
    
    @Override
    public StartMatchInfo getBowlerToStartNewOver(StartMatchInfo startMatchInfo) 
            throws HowzatException {        
        int teamId = startMatchInfo.getBowlingTeamId();
        List<Player> bowlingTeamPlayers = playerService.fetchPlayersFromTeamId(
                                            teamId);
        List<PlayerInfo> bowlingTeamPlayersAsInfo =  CommonUtil.
                convertPlayersToPlayersInfo(bowlingTeamPlayers);
        startMatchInfo.setTeamPlayers(bowlingTeamPlayersAsInfo);
        return startMatchInfo;
    }
    
    public StartMatchInfo getBatsmanToStartNewWicket(
            StartMatchInfo startMatchInfo) throws HowzatException {
        int teamId = startMatchInfo.getBattingTeamId();
        System.out.println(teamId);
        List<Player> battingingTeamPlayers = playerService.fetchPlayersFromTeamId(
                                            teamId);
        List<PlayerInfo> battingingTeamPlayersAsInfo =  CommonUtil.
                convertPlayersToPlayersInfo(battingingTeamPlayers);
        startMatchInfo.setTeamPlayers(battingingTeamPlayersAsInfo);
        return startMatchInfo;
    }
    
    public StartMatchInfo resumeMatchWithNewBatsman(int batsmanId,
            StartMatchInfo startMatchInfo) throws HowzatException {
        Player nextBatsman = playerService.fetchPlayerById(batsmanId);
        startMatchInfo.setFirstBatsmanId(nextBatsman.getId());
        startMatchInfo.setFirstBatsmanName(nextBatsman.getName());
        startMatchInfo.setFirstBatsmanScore(0);
        startMatchInfo.setFirstBatsmanFour(0);
        startMatchInfo.setFirstBatsmanSix(0);
        return startMatchInfo;
    }
}
