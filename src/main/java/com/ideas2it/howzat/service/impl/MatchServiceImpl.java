package com.ideas2it.howzat.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.dao.MatchDAO;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.TeamInfo;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.PlayMatchInfo;
import com.ideas2it.howzat.service.TeamService;
import com.ideas2it.howzat.service.MatchService;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.DateUtil;

@Service
public class MatchServiceImpl implements MatchService{
    
     
    private MatchDAO matchDAO;
    private TeamService teamService;
    private PlayerService playerService;
    
    @Autowired
    public MatchServiceImpl(MatchDAO matchDAO, TeamService teamService,PlayerService playerService){
        this.matchDAO = matchDAO;
        this.teamService = teamService;
        this.playerService = playerService;
    }
    
    
    @Override
    public List<Team> fetchAllTeams() throws HowzatException {
        return teamService.fetchAllTeams();
    } 
    
    @Override
    public List<MatchInfo> fetchAllMatches() throws HowzatException {
        List<Match> matches = matchDAO.viewallMatches();
        return CommonUtil.convertMatchesToMatchInfos(matches); 
    }
    
    @Override
    public MatchInfo createMatch(MatchInfo matchInfo, String date) 
            throws HowzatException {
        Match match = CommonUtil.convertMatchInfoToMatchEntity(matchInfo);
        match.setDate(DateUtil.stringToDate(date));
        match = matchDAO.createMatch(match);
        matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        List<TeamInfo> teamsForMatch = CommonUtil.convertAllTeamsToTeamsInfo(
                                        getTeamsForMatch(match));
        matchInfo.setTeamsForMatch(teamsForMatch);
        return matchInfo;
    } 
    
    @Override
    public void updateMatchByInfo(MatchInfo matchInfo,
            String[] teamsIdToRemove,String[] teamsIdToAdd) throws HowzatException {
        Match match = CommonUtil.convertMatchInfoToMatchEntity(matchInfo);
        updateTeamsToMatch(match, teamsIdToRemove, teamsIdToAdd);
    }
    
    @Override
    public void updateMatchById(int matchId,
            String[] teamsIdToRemove,String[] teamsIdToAdd) 
            throws HowzatException {
        Match match = fetchMatch(matchId);
        updateTeamsToMatch(match, teamsIdToRemove, teamsIdToAdd);
    }

    @Override
    public void updateTeamsToMatch(Match updatedMatch,
            String[] teamsIdToRemove,String[] teamsIdToAdd) 
            throws HowzatException {
        Match matchToBeUpdated = fetchMatch(updatedMatch.getId());
        matchToBeUpdated.setMatchName(updatedMatch.getMatchName());
        matchToBeUpdated.setLocation(updatedMatch.getLocation());
        if ((null != teamsIdToRemove) && (0 < teamsIdToRemove.length)) {
            Set<Team> selectedTeams = new HashSet(fetchTeams(teamsIdToRemove));
            Set<Team> teams = matchToBeUpdated.getTeams();
            for(Team team : selectedTeams) {
                teams.remove(team);
            }
            matchToBeUpdated.setTeams(teams);
        }
        if ((null != teamsIdToAdd) && (0 < teamsIdToAdd.length)) {  
            Set<Team> selectedTeams = new HashSet(fetchTeams(teamsIdToAdd));
            Set<Team> teams = matchToBeUpdated.getTeams();
            teams.addAll(selectedTeams);
            matchToBeUpdated.setTeams(teams);
        }
        matchDAO.updateMatch(matchToBeUpdated);
    }    
    
    @Override     
    public Match fetchMatch(int matchId) throws HowzatException {
        return matchDAO.viewMatch(matchId);    
    }
    
    @Override 
    public List<Team> fetchTeams(String[] teamIds) throws HowzatException {
        return teamService.viewTeams(teamIds);
    }
    
    @Override 
    public List<Player> fetchPlayersFromTeamId(int teamId) throws HowzatException {
        return playerService.fetchPlayersFromTeamId(teamId);
    }
    
    @Override
    public void deleteMatch(int matchId) throws HowzatException {
        Match match = fetchMatch(matchId);
        matchDAO.deleteMatch(match);   
    }
    
    @Override
    public MatchInfo fetchMatchInfo(int matchId, boolean update) 
            throws HowzatException {
        Match match = fetchMatch(matchId);
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);    
        List<Team> matchTeams = new ArrayList(match.getTeams());
        List<TeamInfo> matchTeamInfos = CommonUtil.convertAllTeamsToTeamsInfo(
                                            matchTeams);
        matchInfo.setTeamsSelectedForMatch(new HashSet(matchTeamInfos));
        if(update) {
            List<Team> teamsForMatch = getTeamsForMatch(match);
            List<TeamInfo> teamInfosForMatch = 
                    CommonUtil.convertAllTeamsToTeamsInfo(teamsForMatch);
            matchInfo.setTeamsForMatch(teamInfosForMatch);
        }
        return matchInfo;
    }
    
    @Override
    public List<Team> getTeamsForMatch(Match match) throws HowzatException {
        boolean isSameDate = Boolean.FALSE;
        List<Team> teamsForMatch = new ArrayList<Team>();
        Date dateOfPlay = match.getDate();
        List<Team> allTeams = fetchAllTeams();
        for (Team team : allTeams) {
            //if(team.getStatus()) {           
            for (Match teamsMatch : team.getMatches()) {
                    if (teamsMatch.getDate().equals(dateOfPlay)) {
                        isSameDate = Boolean.TRUE;
                        break;
                    }
                } 
                if (!isSameDate){
                    teamsForMatch.add(team);
                } 
                isSameDate = Boolean.FALSE;
            //}
        }
        return teamsForMatch;
    }  
    
    @Override
    public PlayMatchInfo startMatch(int matchId) throws  HowzatException{
        PlayMatchInfo playMatchInfo = new PlayMatchInfo();
        Match match = fetchMatch(matchId);
        Set<Team> matchTeams = match.getTeams();
        List<Integer> teamIds = new ArrayList<Integer>();
        for(Team team : matchTeams){
            teamIds.add(team.getId());
        }
        List<Player> battingTeamPlayers = fetchPlayersFromTeamId(teamIds.get(0));
        List<Player> bowlingTeamPlayers = fetchPlayersFromTeamId(teamIds.get(1));
        List<PlayerInfo> battingTeamPlayersAsInfo = 
                CommonUtil.convertPlayersToPlayersInfo(battingTeamPlayers);
        List<PlayerInfo> bowlingTeamPlayersAsInfo = 
                CommonUtil.convertPlayersToPlayersInfo(bowlingTeamPlayers);
        List<TeamInfo> teamsPlayingMatch =  
                CommonUtil.convertAllTeamsToTeamsInfo(new ArrayList(matchTeams));
        TeamInfo battingTeam = teamsPlayingMatch.get(0);
        battingTeam.setTeamPlayers(new HashSet(battingTeamPlayersAsInfo));
        TeamInfo bowlingTeam = teamsPlayingMatch.get(1);
        bowlingTeam.setTeamPlayers(new HashSet(bowlingTeamPlayersAsInfo));
        playMatchInfo.setBattingTeamInfo(battingTeam);
        playMatchInfo.setBowlingTeamInfo(bowlingTeam);
        playMatchInfo.setMatchInfo(CommonUtil.convertMatchEntityToMatchInfo(match));
        return playMatchInfo;
    } 
      
}
