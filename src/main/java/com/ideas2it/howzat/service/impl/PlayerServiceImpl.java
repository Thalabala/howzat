package com.ideas2it.howzat.service.impl;

import java.io.BufferedOutputStream;  
import java.io.FileOutputStream;  
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.dao.PlayerDAO;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Contact;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.PlayerNotFoundException;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.PlayerPaginationInfo;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.AgeUtil;
import com.ideas2it.howzat.service.PlayerService;

/**
 * The class which contains all business logics and calculations,
 * process done for player informations.
 *
 */
@Service
public class PlayerServiceImpl implements PlayerService {
    
    private PlayerDAO playerDAO;
    
    @Autowired
    public PlayerServiceImpl(PlayerDAO playerDAO){
        this.playerDAO = playerDAO;
    }
    
    @Override
    public Player isValidId(int playerId) 
            throws HowzatException,PlayerNotFoundException {
        Player player = playerDAO.fetchPlayerById(playerId);
        if (null == player){
            throw new PlayerNotFoundException("Player not present");
        }else{
            return player;
        }     
    }
    
    @Override
    public int totalIdInTable() throws HowzatException {
        return playerDAO.totalIdInTable();
    }
    
    @Override
    public PlayerInfo createPlayer(
            CommonsMultipartFile file, PlayerInfo playerInfo) 
            throws HowzatException, IOException {
        Player player = CommonUtil.convertPlayerInfoToPlayerEntity(playerInfo);
        Contact contact = player.getContact();
        contact.setPlayer(player);
        player.setContact(contact);
        player = uploadPictureURL(file, playerDAO.insertPlayer(player));
        return CommonUtil.convertPlayerEntityToPlayerInfo(player);
    } 
    
    @Override
    public PlayerInfo fetchPlayerInfo(int id) throws HowzatException {
        Player player = playerDAO.fetchPlayerById(id);
        return CommonUtil.convertPlayerEntityToPlayerInfo(player);
    }
    
    @Override
    public Player fetchPlayerById(int id) throws HowzatException {
        return playerDAO.fetchPlayerById(id);
    }
    
    @Override 
    public Player uploadPictureURL(CommonsMultipartFile file ,Player player) 
            throws HowzatException, IOException {   
        if(!file.getOriginalFilename().isEmpty()) {
            String filename = file.getOriginalFilename();
            int playerId = player.getId();
            byte[] bytes = file.getBytes();  
            BufferedOutputStream stream =new BufferedOutputStream(
                    new FileOutputStream(new File("/home/ubuntu/playerimages/" 
                    + playerId + filename)));
            stream.write(bytes);
            stream.flush();
            stream.close();
            String fileUrl = "http://localhost:8080/playerimages/"
                                + playerId + filename;
            player.setDisplayPicture(fileUrl);
            player = playerDAO.updatePlayer(player);
        }
        return player;
    } 
      
    @Override
    public PlayerInfo updatePlayer(
            CommonsMultipartFile file, PlayerInfo playerInfo) 
            throws HowzatException, IOException {  
        Player player = CommonUtil.convertPlayerInfoToPlayerEntity(playerInfo);
        Player playerWithTeam = playerDAO.fetchPlayerById(player.getId());
        player.setTeam(playerWithTeam.getTeam());
        Contact contact = player.getContact();
        player.setContact(contact);
        contact.setPlayer(player);
        uploadPictureURL(file, player);
        playerDAO.updatePlayer(player);
        return CommonUtil.convertPlayerEntityToPlayerInfo(player);
    }
    
    @Override
    public PlayerPaginationInfo fetchAllPlayer(int offset) 
            throws HowzatException { 
        PlayerPaginationInfo playerPaginationInfo = new PlayerPaginationInfo();
        int noOfPages = (int) Math.ceil(totalIdInTable() * 1.0 / 
                            Constants.NO_OF_RECORDS_PER_PAGE);
        List<Player> players = playerDAO.fetchPlayers(offset);
        playerPaginationInfo.setPlayers(
                CommonUtil.convertPlayersToPlayersInfo(players));
        playerPaginationInfo.setNoOfPages(noOfPages);
        return playerPaginationInfo;
    }
    
    @Override
    public void deletePlayer(int playerId) throws HowzatException { 
        Player player = fetchPlayerById(playerId);
        player.setStatus(Boolean.FALSE);
        playerDAO.updatePlayer(player);
    }
    
    @Override
    public List<Player> fetchCountryPlayer(String country) 
            throws HowzatException { 
        return playerDAO.fetchCountryPlayers(country);
    }
   
    @Override
    public List<Player> fetchPlayersFromId(String[] playerIds) 
            throws HowzatException {
        List<Integer> playerIdsToBeFetched = new ArrayList<Integer>();
        for(String playerId : playerIds){
            playerIdsToBeFetched.add(Integer.parseInt(playerId));
        }
        return playerDAO.fetchPlayersFromId(playerIdsToBeFetched);
    }
    
    @Override
    public List<Player> fetchPlayersFromTeamId(int teamId) 
            throws HowzatException {
        return playerDAO.fetchPlayersFromTeamId(teamId);
    }
    
    @Override
    public JSONArray fetchPlayersByPage(int page) throws HowzatException {
        PlayerPaginationInfo playerPaginationInfo = fetchAllPlayer((page-1) * 
                                    Constants.NO_OF_RECORDS_PER_PAGE);
        List<PlayerInfo> players = playerPaginationInfo.getPlayers();
        JSONArray array = new JSONArray();
        for (PlayerInfo playerInfo : players) { 
            JSONObject jsonPlayer = new JSONObject();
            jsonPlayer.put(Constants.PLAYER_ID, playerInfo.getId());
            jsonPlayer.put(Constants.NAME, playerInfo.getName());
            jsonPlayer.put(Constants.DATE_OF_BIRTH, playerInfo.getDateOfBirth());
            jsonPlayer.put(Constants.COUNTRY, playerInfo.getCountry());
            jsonPlayer.put(Constants.PLAYER_TYPE, playerInfo.getPlayerType());
            array.put(jsonPlayer);
        }
        return array;
    } 
}

