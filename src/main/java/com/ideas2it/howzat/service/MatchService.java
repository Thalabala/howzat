package com.ideas2it.howzat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.dao.MatchDAO;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.PlayMatchInfo;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.DateUtil;

/**
 * The class which contains all business logics and calculations,
 * process done for match informations.
 *
 */
public interface MatchService {  
   
    /**
     * Fetches all Team objects from teamDAO via teamService
     * 
     * @returns List<Team> - List contains all team objects 
     */
    public List<Team> fetchAllTeams() throws HowzatException;
    
    /**
     * Fetches all matches and returns as object.
     *   
     * @ returns List<Match> - List of all match objects 
     */
    public List<MatchInfo> fetchAllMatches() throws HowzatException;
    
    /**
     * Match inputs are setted into object
     *
     * @param name - name given to match
     * @param location - location given to match
     * @param date - date given to match
     * @param matchType - matchType given to match
     * @return matchInfo - the object contains match and screened teams
     */
    public MatchInfo createMatch(MatchInfo matchInfo, String date) throws HowzatException;
    
    public void updateMatchByInfo(MatchInfo matchInfo,
            String[] teamsIdToRemove,String[] teamsIdToAdd) throws HowzatException;
    
    public List<Team> getTeamsForMatch(Match match) throws HowzatException;
    /** 
     * Gets update inputs (matchname and location),
     * Fetches teams of id's  
     * removes teams from teams in match (if teamsIdToRemove not null)
     * adds teams to teams in match (if teamsIdToAdd not null)
     * set updated match object to DAO
     * 
     * @param matchId - id of the match to be updated
     * @param teamsIdToRemove - array containing id's of teams to be removed
     * @param teamsIdToAdd - array containing id's of teams to be added
     * @param matchLocation - updated match location
     * @param matchName - updated match name
     */ 
    public void updateMatchById(int matchId,
            String[] teamsIdToRemove,String[] teamsIdToAdd) 
            throws HowzatException;
            
    /** 
     * Gets update inputs (matchname and location),
     * Fetches teams of id's  
     * removes teams from teams in match (if teamsIdToRemove not null)
     * adds teams to teams in match (if teamsIdToAdd not null)
     * set updated match object to DAO
     * 
     * @param matchId - id of the match to be updated
     * @param teamsIdToRemove - array containing id's of teams to be removed
     * @param teamsIdToAdd - array containing id's of teams to be added
     * @param matchLocation - updated match location
     * @param matchName - updated match name
     */
    public void updateTeamsToMatch(Match match,
            String[] teamsIdToRemove,String[] teamsIdToAdd) 
            throws HowzatException;
    
    /**
     * Gets match id and passes to DAO to fetch match object
     *
     * @param matchId - id of match object to be fetched
     * @return Match - corresponding match object to matchId
     */     
    public Match fetchMatch(int matchId) throws HowzatException;
    
    /**
     * Gets teamId's as string and passes to teamservice to fetch team objects
     *
     * @param teamIds - ids of team objects to be fetched
     * @return List<Team>
     */ 
    public List<Team> fetchTeams(String[] teamIds) throws HowzatException;
    
    /**
     * Gets match id ,fetches respective object 
     * passes object to delete match in dao
     *
     * @param matchId - id of match to be deleted
     */
    public void deleteMatch(int matchId) throws HowzatException;
    
    /**
     * Compares match date with teams having other matches date and screens team
     *
     * @return matchInfo - the object contains match and screened teams
     */
    public MatchInfo fetchMatchInfo(int matchId, boolean update) throws HowzatException;
    public PlayMatchInfo startMatch(int matchId) throws HowzatException;
    public List<Player> fetchPlayersFromTeamId(int teamIds) throws HowzatException;
           
}
