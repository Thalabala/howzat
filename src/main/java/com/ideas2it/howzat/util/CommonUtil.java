package com.ideas2it.howzat.util; 

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import com.ideas2it.howzat.entities.Contact;
import com.ideas2it.howzat.entities.Match;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.TeamInfo;
import com.ideas2it.howzat.info.UserInfo;

public class CommonUtil {

    /**
     * Team pojo object is converted into TeamInfo by fetching from team and 
     * setting them into TeamInfo
     * 
     * @param team - entity object which to be converted to Info
     * @return teamInfo - converted info object 
     */      
    public static TeamInfo convertTeamEntityToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        teamInfo.setId(team.getId());
        if(null != team.getTeamName()){
            teamInfo.setTeamName(team.getTeamName());
        }
        if(null != team.getCountry()){
            teamInfo.setCountry(team.getCountry());
        }
        return teamInfo;
    }
  
    /**
     * TeamInfo is converted into Team pojo object by fetching from TeamInfo and 
     * setting them into Team
     * 
     * @param teamInfo - converted info object
     * @return team - entity object which will be converted to Info
     */
    public static Team convertTeamInfoToTeamEntity(TeamInfo teamInfo) {
        Team team = new Team();
        team.setId(team.getId());
        if(null != teamInfo.getTeamName()){
            team.setTeamName(teamInfo.getTeamName());
        }
        if(null != teamInfo.getCountry()){
            team.setCountry(teamInfo.getCountry());
        }
        
        return team;
    }
    
    /**
     * Player pojo object is converted into PlayerInfo by fetching from player and 
     * setting them into playerInfo
     * 
     * @param player - entity object which to be converted to Info
     * @return playerInfo - converted info object 
     */
    public static PlayerInfo convertPlayerEntityToPlayerInfo(Player player){
        PlayerInfo playerInfo = new PlayerInfo();
        Contact contact = player.getContact();
        playerInfo.setId(player.getId());
        playerInfo.setContactId(contact.getId());
        playerInfo.setPincode(contact.getPincode());
        if(null != player.getCountry()){
            playerInfo.setCountry(player.getCountry());
        }
        if(null != player.getBattingType()){
            playerInfo.setBattingType(player.getBattingType());
        }
        if(null != player.getBowelingType()){
            playerInfo.setBowelingType(player.getBowelingType());
        }
        if(null != player.getDateOfBirth()){
            playerInfo.setDateOfBirth(player.getDateOfBirth());
        }
        if(null != player.getName()){
            playerInfo.setName(player.getName());
        }
        if(null != player.getPlayerType()){
            playerInfo.setPlayerType(player.getPlayerType());
        }
        if(null != player.getDisplayPicture()){
            playerInfo.setDisplayPicture(player.getDisplayPicture());
        }
        if(null != player.getAge()){
            playerInfo.setAge(player.getAge());
        }
        if(null != contact.getAddress()){
            playerInfo.setAddress(contact.getAddress());
        }
        if(null != contact.getContactNumber()){
            playerInfo.setContactNumber(contact.getContactNumber());
        }
        return playerInfo;
    }
    
    /**
     * PlayerInfo is converted into Player pojo object by fetching from playerInfo  
     * and setting them into player
     * 
     * @param playerInfo - converted info object
     * @return player - entity object which will be converted to Info
     */
    public static Player convertPlayerInfoToPlayerEntity(PlayerInfo playerInfo){
        Player player = new Player();
        Contact contact = new Contact();
        player.setId(playerInfo.getId());
        contact.setId(playerInfo.getId());
        contact.setPincode(playerInfo.getPincode());
        if(null != playerInfo.getCountry()){
            player.setCountry(playerInfo.getCountry());
        }
        if(null != playerInfo.getBattingType()){
            player.setBattingType(playerInfo.getBattingType());
        }
        if(null != playerInfo.getBowelingType()){
            player.setBowelingType(playerInfo.getBowelingType());
        }
        if(null != playerInfo.getDateOfBirth()){
            player.setDateOfBirth(playerInfo.getDateOfBirth());
        }
        if(null != playerInfo.getName()){
            player.setName(playerInfo.getName());
        }
        if(null != playerInfo.getPlayerType()){
            player.setPlayerType(playerInfo.getPlayerType());
        }
        if(null != playerInfo.getDisplayPicture()){
            player.setDisplayPicture(playerInfo.getDisplayPicture());
        }
        if(null != playerInfo.getAddress()){
            contact.setAddress(playerInfo.getAddress());
        }
        if(null != playerInfo.getContactNumber()){
            contact.setContactNumber(playerInfo.getContactNumber());
        }
        contact.setPlayer(player);
        player.setContact(contact);
        return player;
    }  
    
    /**
     * Match pojo object is converted into matchInfo by fetching from match and 
     * setting them into matchInfo
     * 
     * @param match - entity object which to be converted to Info
     * @return matchInfo - converted info object 
     */
    public static MatchInfo convertMatchEntityToMatchInfo(Match match) {
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setId(match.getId());
        if(null != match.getMatchName()) {
            matchInfo.setMatchName(match.getMatchName());
        }
        if(null != match.getLocation()) {
            matchInfo.setLocation(match.getLocation());
        }
        if(null != match.getDate()) {
            matchInfo.setDate(match.getDate());
        }
        if(null != match.getMatchType()) {
            matchInfo.setmatchType(match.getMatchType());
        }
        return matchInfo;
    }
    
    /**
     * MatchInfo is converted into match pojo object by fetching from matchInfo  
     * and setting them into match
     * 
     * @param matchInfo - converted info object
     * @return match - entity object which will be converted to Info
     */
    public static Match convertMatchInfoToMatchEntity(MatchInfo matchInfo) {
        Match match = new Match();
        match.setId(matchInfo.getId());
        if(null != matchInfo.getMatchName()) {
            match.setMatchName(matchInfo.getMatchName());
        }
        if(null != matchInfo.getLocation()) {
            match.setLocation(matchInfo.getLocation());
        }
        if(null != matchInfo.getDate()) {
            match.setDate(matchInfo.getDate());
        }
        if(null != matchInfo.getMatchType()) {
            match.setmatchType(matchInfo.getMatchType());
        }
        return match;
    }  
    
    /**
     * User pojo object is converted into userInfo by fetching from user and 
     * setting them into userInfo
     * 
     * @param user - entity object which to be converted to Info
     * @return userInfo - converted info object 
     */
    public static UserInfo convertUserEntityToUserInfo(User user){
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setAttempts(user.getAttempts());
        if(null != user.getGender()){
            userInfo.setGender(user.getGender());
        }
        if(null != user.getEmailId()){
            userInfo.setEmailId(user.getEmailId());
        }
        if(null != user.getRole()){
            userInfo.setRole(user.getRole());
        }
        if(null != user.getPassword()){
            userInfo.setPassword(user.getPassword());
        }
        if(null != user.getName()){
            userInfo.setName(user.getName());
        }
        if(null != user.getContactNumber()){
            userInfo.setContactNumber(user.getContactNumber());
        }
        return userInfo;
    }
  
    /**
     * userInfo is converted into user pojo object by fetching from userInfo  
     * and setting them into user
     * 
     * @param userInfo - converted info object
     * @return user - entity object which will be converted to Info
     */
    public static User convertUserInfoToUserEntity(UserInfo userInfo){
        User user = new User();
        user.setId(userInfo.getId());
        user.setAttempts(userInfo.getAttempts());
        if(null != userInfo.getGender()){
            user.setGender(userInfo.getGender());
        }
        if(null != userInfo.getEmailId()){
            user.setEmailId(userInfo.getEmailId());
        }
        if(null != userInfo.getRole()){
            user.setRole(userInfo.getRole());
        }
        if(null != userInfo.getPassword()){
            user.setPassword(userInfo.getPassword());
        }
        if(null != userInfo.getName()){
            user.setName(userInfo.getName());
        }
        if(null != userInfo.getContactNumber()){
            user.setContactNumber(userInfo.getContactNumber());
        }
        return user;
    }
    
    /**
     * List of Player pojo objects are converted into
     * List of playerInfo by calling convertPlayerEntityToPlayerInfo method
     * 
     * @param players - List of entity object which to be converted to Info
     * @return playersInfo - converted List of info object 
     */
    public static List<PlayerInfo> convertPlayersToPlayersInfo(
            List<Player> players) {
        List<PlayerInfo> playersInfo = new ArrayList<PlayerInfo>();
        for(Player player : players) {
            playersInfo.add(convertPlayerEntityToPlayerInfo(player));
        }
        return playersInfo;
    }
    
    /**
     * List of Team pojo objects are converted into
     * List of TeamInfo by calling convertTeamEntityToTeamInfo method
     * 
     * @param teams - List of entity object which to be converted to Info
     * @return teamsInfo - converted List of info object 
     */
    public static List<TeamInfo> convertAllTeamsToTeamsInfo(
            List<Team> teams) {
        List<TeamInfo> teamsInfo = new ArrayList<TeamInfo>();
        for(Team team : teams) {
            teamsInfo.add(convertTeamEntityToTeamInfo(team));
        }
        return teamsInfo;
    }
    
    /**
     * List of Match pojo objects are converted into
     * List of matchInfo by calling convertMatchEntityToMatchInfo method
     * 
     * @param matches - List of entity object which to be converted to Info
     * @return matchInfos - converted List of info object 
     */
    public static List<MatchInfo> convertMatchesToMatchInfos(
            List<Match> matches) {
        List<MatchInfo> matchInfos = new ArrayList<MatchInfo>();
        for(Match match : matches) {
            matchInfos.add(convertMatchEntityToMatchInfo(match));
        }
        return matchInfos;
    }      
    
    /**
     * Generates a random number between o to upperlimit and 
     * return a integer as per priority conditions.
     * 
     * @param upperRange - till which a random number can be genereted
     * @return  prioritizedRun - run generated as per priority
     */
    public static int fetchPrioritizedRandomRuns(int upperRange) {
        Random random = new Random();
        int generetedRun = random.nextInt(upperRange);
        int prioritizedRun = 0;
        if(0 == generetedRun) {
            prioritizedRun = 0;
        } else if(1 == generetedRun) {
            prioritizedRun = 1;
        } else if(2 == generetedRun) {
            prioritizedRun = 2;
        } else if(3 == generetedRun) { 
            prioritizedRun = 3;
        } else if(4 == generetedRun) {
            prioritizedRun = 4;
        } else if(5 == generetedRun) {
            prioritizedRun = 5;
        } else if(6 == generetedRun) {
            prioritizedRun = 6;
        } else if((7 <= generetedRun) && (generetedRun <= 12)) {
            prioritizedRun = 1;
        } else if((13 <= generetedRun) && (generetedRun<= 17)) {
            prioritizedRun = 1;
        } else if((18 <= generetedRun) && (generetedRun <= 20)) { 
            prioritizedRun = 2;
        } else if((21 <= generetedRun) && (generetedRun <= 24)) {
            prioritizedRun = 11;
        } else if((25 <= generetedRun) &&(generetedRun <= 27)) {
            prioritizedRun = 7;
        } else if((28 <= generetedRun) && (generetedRun <= 29)) {
            prioritizedRun = 8;
        } else if((30 <= generetedRun) &&(generetedRun <= 31)) {
            prioritizedRun = 9;
        } else if((32 <= generetedRun) && (generetedRun <= 33)) {
            prioritizedRun = 10;
        } 
        return prioritizedRun;
    }
}
