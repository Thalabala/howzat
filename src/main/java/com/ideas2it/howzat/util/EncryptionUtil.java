package com.ideas2it.howzat.util;

import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.lang.StringBuilder;

import org.apache.log4j.Logger;

public class EncryptionUtil {
     
    private static final Logger logger = Logger.getLogger(EncryptionUtil.class);
    
    /**
     * Encrypting the password entered by user using MD5 encryption
     * 
     * @param String password - password entered by user
     * @return String - encrypted Password
     */
    public static String encryptPassword(String password) {
        String encryptedPassword = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getBytes());
            byte[] bytes = messageDigest.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for(int index =0; index < bytes.length ;index++)
            {
                stringBuilder.append(Integer.toString((
                        bytes[index] & 0xff) + 0x100, 16).substring(1));
            }
            encryptedPassword = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.error("error at encrypting password" + e);
        }
        return encryptedPassword;
    }
    
}   
