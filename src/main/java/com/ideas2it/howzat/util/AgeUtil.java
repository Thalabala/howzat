package com.ideas2it.howzat.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 
 */
public class AgeUtil {
     
    static String age = "";    
    /**
     * This is a method uses simple date format and calender
     * and find the players age
     * using their date of birth as input
     * @param 
     */
    public static String calculateAge(String DOBInput) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        int ageInYear;
        try {
            Date date = sdf.parse(DOBInput);
            Calendar presentDate = Calendar.getInstance();
            Calendar dob = Calendar.getInstance();
            dob.setTime(date);
            ageInYear = presentDate.get(Calendar.YEAR) 
                    - dob.get(Calendar.YEAR);
            int ageInMonth = (presentDate.get(Calendar.MONTH) 
                    <= dob.get(Calendar.MONTH)) ? (dob.get(Calendar.MONTH)) 
                    - (presentDate.get(Calendar.MONTH)) 
                    : (presentDate.get(Calendar.MONTH) - dob.get(Calendar.MONTH));
            int ageInDays = (presentDate.get(Calendar.DAY_OF_MONTH) 
                    <= dob.get(Calendar.DAY_OF_MONTH)) 
                    ? (dob.get(Calendar.DAY_OF_MONTH) 
                    - presentDate.get(Calendar.DAY_OF_MONTH))   
                    : (presentDate.get(Calendar.DAY_OF_MONTH) 
                    - dob.get(Calendar.DAY_OF_MONTH));
            age = ageInYear + " Years " + ageInMonth + " months " + ageInDays 
                    + " days ";
            return age;
        }catch (ParseException e) {
            return age = "error";
        }
    } 
    
    /**
     * only returns age in year month date 
     *
     * @returns String age - age in year month date format 
     */
    public static String retriveAge() {
        return age;
    }
    
}   
