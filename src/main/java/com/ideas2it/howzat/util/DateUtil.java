package com.ideas2it.howzat.util;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {

    public static Date stringToDate(String matchDate) {
        Date date = null;
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient(false);
            date = sdf.parse(matchDate);
            Date today = new Date();
            if (0 > date.compareTo(today)) {
                return null;
            }
        } catch (ParseException e) {
            return date; 
        }
        return date;    
    }
}
