package com.ideas2it.howzat.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.OverInfo;
import com.ideas2it.howzat.info.OverDetailInfo;
import com.ideas2it.howzat.service.OverDetailService;

/**
 * Contains the information of the ball bowled.
 * Has info like bowler,batsman,runs etc for single ball.
 */
@Controller
public class OverDetailController {
    
    public OverDetailService overDetailService;
    public static final Logger logger = Logger
            .getLogger(OverDetailController.class);

    @Autowired
    public OverDetailController (OverDetailService overDetailService) {
        this.overDetailService = overDetailService;
    }
    
    @RequestMapping(value = "/playBall", method = RequestMethod.GET)
    public void savePlayersToPlay(HttpServletRequest request, 
            HttpServletResponse response) {
        try {
            int batsmanId = Integer.parseInt(
                                    request.getParameter("strickerId"));
            int bowlerId = Integer.parseInt(
                                    request.getParameter("bowlerId"));  
            int matchId = Integer.parseInt(
                                    request.getParameter("matchId"));
            int overId = Integer.parseInt(
                                    request.getParameter("overId"));
            JSONObject scoreCard = overDetailService
                                               .getPlayersToStart(batsmanId,
                                               bowlerId, matchId, overId);
            response.setContentType("application/json");
            response.getWriter().write(scoreCard.toString());
        } catch(HowzatException | IOException e) {
            logger.error(e.getMessage());
        }
    }
}
