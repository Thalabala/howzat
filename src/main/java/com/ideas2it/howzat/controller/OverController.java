package com.ideas2it.howzat.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.OverInfo;
import com.ideas2it.howzat.info.StartMatchInfo;
import com.ideas2it.howzat.service.OverService;

/**
 * Contains the information of the ball bowled.
 * Has info like bowler,batsman,runs etc for single ball
 *
 */
@Controller
public class OverController {
    
    public OverService overService;
    public static final Logger logger = Logger.getLogger(OverController.class);

    @Autowired
    public OverController (OverService overService) {
        this.overService = overService;
    }
    
    /**
     * Gets batsmans, bowlers and match ids to set and create a over
     * 
     * @Param  batsmanIds - the selected batsmans id's to play.
     * @Param  bowlerId - the selected bowler id to play.
     * @Param  matchId - the match id in which this over to be played.
     */
    @RequestMapping(value = "/savePlayers", method = RequestMethod.POST)
    public String savePlayersToPlay(@RequestParam("batsmans") 
            String[] batsmanIds, @RequestParam("bowler") 
            String[] bowlerId, @RequestParam("matchId") int matchId,
            Model model) {
        try {
            StartMatchInfo startMatchInfo = overService.getPlayersToStart(
                                                batsmanIds, bowlerId, matchId);
            model.addAttribute("startMatchInfo", startMatchInfo);
        } catch(HowzatException e) {
            logger.error(e.getMessage());
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return "playMatch";
    }
    
    /**
     *
     *
     *
     */
    @RequestMapping(value = "/changePlayer", method = RequestMethod.POST)
    public String changePlayer(@ModelAttribute StartMatchInfo startMatchInfo
            ,@RequestParam("choosePlayer") int choosePlayer, Model model) {
        String redirectedPage = "";
        System.out.println(choosePlayer);
        try {
            if(0 == choosePlayer) {
                startMatchInfo = overService.getBowlerToStartNewOver(
                                                startMatchInfo);
                redirectedPage = "chooseNewBowler";
            } else {
                startMatchInfo = overService.getBatsmanToStartNewWicket(
                                                startMatchInfo);
                redirectedPage = "chooseNewBatsman";
            }
            model.addAttribute("startMatchInfo", startMatchInfo);
        } catch(HowzatException e) {
            logger.error(e.getMessage());
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return redirectedPage;
    }
    
    @RequestMapping(value = "/startNewOver", method = RequestMethod.POST)
    public String changeBowlerForNextOver(@ModelAttribute StartMatchInfo startMatchInfo
            ,@RequestParam("bowler") String[] bowlerId, Model model) {
        try {
            String[] batsmanIds = 
                    {Integer.toString(startMatchInfo.getFirstBatsmanId())
                    ,Integer.toString(startMatchInfo.getSecondBatsmanId())};
            startMatchInfo = overService.getPlayersToStart(
                             batsmanIds, bowlerId, startMatchInfo.getMatchId());
            model.addAttribute("startMatchInfo", startMatchInfo);
        } catch(Exception e) {
            logger.error(e.getMessage());
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return "playMatch";
    }
    
    @RequestMapping(value = "/nextBatsman", method = RequestMethod.POST)
    public String changeBatsman(@ModelAttribute StartMatchInfo startMatchInfo
            ,@RequestParam("batsman") String[] batsmanIds, Model model) {
        try {
            int batsmanId = Integer.parseInt(batsmanIds[0]);
            startMatchInfo = overService.resumeMatchWithNewBatsman(batsmanId,
                                startMatchInfo);
            model.addAttribute("startMatchInfo", startMatchInfo);
        } catch(Exception e) {
            logger.error(e.getMessage());
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return "playMatch";
    }
    
}
