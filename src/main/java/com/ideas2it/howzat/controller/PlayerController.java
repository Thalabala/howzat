package com.ideas2it.howzat.controller;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.info.PlayerInfo;
import com.ideas2it.howzat.info.PlayerPaginationInfo;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.service.PlayerService;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.PlayerNotFoundException;

import org.apache.log4j.Logger;

/**
 * Acts as Servlet, Receives request from server, 
 * Fetches informations from request and redirects to methods
 * and give response to server
 */
@Controller
public class PlayerController {

    private static final Logger logger = Logger.getLogger(
                                            PlayerController.class);
                                            
    private PlayerService playerService;
    
    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }
    
    @RequestMapping(value = "/newPlayer", method = RequestMethod.GET)
    private String createPlayer(Model model) throws HowzatException {
        PlayerInfo playerInfo = new PlayerInfo();
        model.addAttribute("playerInfo",playerInfo);
        return "addplayer";
    }
        
    /**
     * Gets input for required fields of player,validate them, 
     * stores in a string and passes to service layer method
     *  
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/createPlayer", method = RequestMethod.POST)
    private String addPlayer(@RequestParam CommonsMultipartFile file, 
            @ModelAttribute PlayerInfo playerInfo, Model model) 
            throws IOException , HowzatException {
        playerInfo = playerService.createPlayer(file, playerInfo);
        model.addAttribute("playerInfo",playerInfo);
        return "viewplayer";
    } 
       
    /** 
     * Edit operation get id number
     * fetches the needed informations and redirects to editPlayer page
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/editPlayer", method = RequestMethod.GET)
    private String editPlayer(
            @RequestParam(Constants.PLAYER_ID) int id, Model model) 
            throws HowzatException, IOException, ServletException {
        PlayerInfo playerInfo = playerService.fetchPlayerInfo(id);
        model.addAttribute("playerInfo",playerInfo);
        return "editplayer";
    }

    /**
     * Gets input for required fields of player,
     * stores in a string and passes to service layer method
     *  
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/updatePlayer", method = RequestMethod.POST)
    private String updatePlayer(@RequestParam CommonsMultipartFile file, 
            @ModelAttribute PlayerInfo playerInfo, Model model) 
            throws ServletException,IOException, HowzatException {
        playerInfo = playerService.updatePlayer(file, playerInfo);
        model.addAttribute("playerInfo",playerInfo);
        return "viewplayer";
    }
    
    /** 
     * Displays single player using id of player
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/viewPlayer", method = RequestMethod.POST)
    private String displayPlayer(
            @RequestParam(Constants.PLAYER_ID) int id, Model model) 
            throws HowzatException, IOException, ServletException {
        PlayerInfo playerInfo = playerService.fetchPlayerInfo(id);
        model.addAttribute("playerInfo",playerInfo);
        return "viewplayer";
    }

    /** 
     * Display all Player page by page using 
     *
     * @param request - request containing values from localhost
     * @param response - response for the locaddplayeralhost request 
     */
    @RequestMapping(value = "/viewPlayers", method = RequestMethod.GET)
    private String displayAllPlayers(Model model) 
            throws HowzatException, IOException, ServletException {
        PlayerPaginationInfo playerPaginationInfo = playerService.fetchAllPlayer(0);
        model.addAttribute("playerPaginationInfo", playerPaginationInfo);
        model.addAttribute(Constants.CURRENT_PAGE, 1);
        return "viewallplayers"; 
    }
    
    /**
     * Method for hard delete from database
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/deletePlayer", method = RequestMethod.GET)
    private String deletePlayer(
            @RequestParam(Constants.PLAYER_ID) int id, Model model) 
            throws IOException,ServletException, HowzatException {
        playerService.deletePlayer(id);
        return "redirect:/viewPlayers";
    } 
    
    /**
     * Fetches player objects
     * converts them into json object array and returns 
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/viewPlayersPageWise", method = RequestMethod.GET)
    private void viewPlayers(
            HttpServletRequest request, HttpServletResponse response)
            throws IOException,ServletException, HowzatException {
        int page = Integer.parseInt(request.getParameter(Constants.PAGE));
        String playersInfo = (playerService.fetchPlayersByPage(page)).toString();
        response.setContentType("application/json");
        response.getWriter().write(playersInfo);
    }
    
    /**
     * Redirect user to error page and display the error
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     * @param String error - error to be displayed to user 
     *
    private void redirectToErrorPage (
            HttpServletRequest request, HttpServletResponse response,
            String error) {
        try {
            request.setAttribute(Constants.ERROR, error);
            RequestDispatcher dispatcher = request.getRequestDispatcher(
                                            "error.jsp");            
            dispatcher.forward(request, response);
        } catch (IOException | ServletException e) {
            logger.error(Constants.ERROR_AT_REDIRECT_PAGE);
        }
    } */
}
