package com.ideas2it.howzat.controller;

import java.io.IOException;
import java.util.Set;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.PlayMatchInfo;
import com.ideas2it.howzat.util.CommonUtil;
import com.ideas2it.howzat.util.DateUtil;
import com.ideas2it.howzat.service.MatchService;

/**
 * Acts as Servlet, Receives request from server, 
 * Fetches informations from request and redirects to methods
 * and give response to server
 */
@Controller
public class MatchController {

    private MatchService matchService;

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

    private static final Logger logger = Logger.getLogger(MatchController.class);
   
    /**
     * Creates matchInfo object, added in model attribute and 
     * redirects to addmatch page
     * 
     * @param model - This for the next page under a different name.
     * @return addmatch - Redirecting to addmatch jsp page.
     */ 
    @RequestMapping(value = "/newMatch", method = RequestMethod.GET)
    private String addMatch(Model model) { 
        MatchInfo matchInfo = new MatchInfo();
        model.addAttribute("matchInfo", matchInfo);
        return "addmatch";
    }
    
    /**
     * Gets input for required fields of match, 
     * stores in a string and passes to service layer method
     *  
     * @param request - request containing values from localhost
     * @param response - response for the localhost request 
     * @throws ServletException - 
     */
    @RequestMapping(value = "/createMatch", method = RequestMethod.POST)
    private String createMatch(@ModelAttribute MatchInfo matchInfo, 
            @RequestParam("matchDate") String date, Model model) {
        try {
            matchInfo = matchService.createMatch(matchInfo, date);
            model.addAttribute("matchInfo", matchInfo);
            return "addteamtomatch";
        } catch (HowzatException exception) {
            model.addAttribute("error", exception.getMessage());
            return "error";
        }
    }
    
    /**
     * Gets selected teamIds and passes to service to add teams to match
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/addTeamToMatch", method = RequestMethod.POST)
    private String addTeamToMatch(
            @RequestParam("selectedteams") String[] teamIdsToAdd, 
            @RequestParam("matchid") int matchId, Model model) {
        try {
                matchService.updateMatchById(matchId, null, teamIdsToAdd);
                return "redirect:/viewMatch?matchId=" + matchId;
        } catch (HowzatException exception) {
            model.addAttribute("error", exception.getMessage());
            return "error";
        } /*catch (ServletException | IOException exception) {
            logger.error(Constants.ERROR_AT_REDIRECT_PAGE + exception);
            model.addAttribute("error", exception.getMessage());
            return "error";
        }*/
    }
      
    /** 
     * Displays selected match details and selected teams of the match
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/viewMatch", method = RequestMethod.GET)
    private String displayMatch(@RequestParam("matchId") int matchId,
            Model model) 
            throws ServletException, IOException, HowzatException {
        MatchInfo matchInfo = matchService.fetchMatchInfo(
                                matchId, Boolean.FALSE);
        model.addAttribute("matchInfo", matchInfo);
        return "viewmatch";
    }
    
    /** 
     * Displays all match details fetched from db 
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/viewAllMatches", method = RequestMethod.GET)
    private String displayAllMatches(Model model) 
            throws ServletException, IOException, HowzatException {
        List<MatchInfo> matches = matchService.fetchAllMatches();
        model.addAttribute("matches", matches);
        return "viewallmatches";
    }
    
    /** 
     * Edit operation fetches match ,sets object and team players to request and 
     * forward to js page 
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/editMatch", method = RequestMethod.GET)
    private String editMatch(@RequestParam("matchId") int matchId, Model model) 
            throws ServletException, IOException, HowzatException {
        MatchInfo matchInfo = matchService.fetchMatchInfo(matchId, Boolean.TRUE);
        model.addAttribute("matchInfo",matchInfo);
        return "editmatch";
    }

    /** 
     * Adds and removes selected teams in match and updates
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/updateMatch", method = RequestMethod.POST)
    private String updateMatch(
            @RequestParam(required=false,name="removeteams") 
            String[] teamsIdToRemove, @RequestParam(
            required=false,name="addteams") String[] teamsIdToAdd,
            @ModelAttribute MatchInfo matchInfo, Model model) 
            throws ServletException, IOException, HowzatException {
        matchService.updateMatchByInfo(matchInfo, teamsIdToRemove,
                teamsIdToAdd);
        return "redirect:/viewMatch?matchId=" + matchInfo.getId();
    }
   
    /** 
     * Deletes the selected team
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/deleteMatch", method = RequestMethod.GET)
    private String deleteMatch(
            @RequestParam("matchId") int matchId, Model model) 
            throws ServletException, IOException, HowzatException {
        matchService.deleteMatch(matchId);
        return "redirect:/viewAllMatches";
    }
    
    
    @RequestMapping(value = "/startMatch", method = RequestMethod.GET)
    private String startMatch(
            @RequestParam("matchId") int matchId, Model model) 
            throws ServletException, IOException, HowzatException {
        PlayMatchInfo playMatchInfo = matchService.startMatch(matchId);
        model.addAttribute("playMatchInfo",playMatchInfo);
        return "selectPlayersToPlay";
    }
    
    /**
     * Redirect user to error page
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     * @param String error - error to be displayed to user 
     
    private void redirectToErrorPage (
            HttpServletRequest request, HttpServletResponse response,
            String error) {
        try {
            request.setAttribute(Constants.ERROR, error);
            RequestDispatcher dispatcher = request.getRequestDispatcher(
                                            "error.jsp");            
            dispatcher.forward(request, response);
        } catch (IOException | ServletException e) {
            
        }
    }*/ 
}
