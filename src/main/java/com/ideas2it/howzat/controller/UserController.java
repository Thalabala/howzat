package com.ideas2it.howzat.controller;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.entities.User;
import com.ideas2it.howzat.exceptions.DeactiveUserException;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.exceptions.InvalidUserException;
import com.ideas2it.howzat.exceptions.PasswordMismatchException;
import com.ideas2it.howzat.info.UserInfo;
import com.ideas2it.howzat.service.UserService;
import com.ideas2it.howzat.util.EncryptionUtil;


/**
 * Acts as Servlet, Receives request from server, 
 * Fetches informations from request and redirects to methods
 * and give response to server
 */
@Controller
public class UserController {

    private static final Logger logger = Logger.getLogger(UserController.class);
    
    private UserService userService;
    
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    
    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    private String addUser(Model model) throws HowzatException {
        UserInfo userInfo = new UserInfo();
        model.addAttribute("userInfo", userInfo);
        return "adduser";
    }
        
    /**
     * Gets input for required fields of user, 
     * stores in a string and passes to service layer method
     *  
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    private String createUser(@ModelAttribute UserInfo userInfo, Model model) {
        try {
            userService.createUser(userInfo);
            model.addAttribute(Constants.ERROR, "User created Succesfully");
            return "index";
        } catch (HowzatException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return "error";
        }
    }
    
    /**
     * Validating user by comparing encrypted password
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/validateUser", method = RequestMethod.POST)
    private String validateUser(HttpServletRequest request) {
        String redirectedPage = "index";
        try {
            String emailId = request.getParameter(Constants.EMAIL_ID);
            String encryptedPassword = EncryptionUtil.encryptPassword(
                                        request.getParameter(
                                        Constants.PASSWORD));
            if(userService.authendicateUser(emailId,encryptedPassword)) {
                HttpSession session = request.getSession();
                session.setAttribute(Constants.EMAIL_ID,emailId);
                redirectedPage = "home";
            }
        } catch (InvalidUserException e) {
            logger.error("E-mail not exist in database " + e.getMessage());
            request.setAttribute(Constants.ERROR, e.getMessage());
        } catch (PasswordMismatchException e) {
            logger.error(e.getMessage());
            request.setAttribute(Constants.ERROR, e.getMessage());
        } catch (DeactiveUserException e) {
            logger.error(e.getMessage());
            request.setAttribute(Constants.ERROR,e.getMessage());
        } catch (HowzatException e) {
            logger.error(e.getMessage());
            request.setAttribute(Constants.ERROR,e.getMessage());
        } finally {
            return redirectedPage;        
        }
    }
    
    /**
     * Gets input for required fields of user, 
     * stores in a string and passes to service layer method
     *  
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/logoutUser", method = RequestMethod.GET)
    private String logout(HttpServletRequest request) {
        HttpSession session = request.getSession(Boolean.FALSE);
        session.invalidate();
        return "index";
    }
}
