package com.ideas2it.howzat.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.exceptions.HowzatException;
import com.ideas2it.howzat.info.TeamInfo;
import com.ideas2it.howzat.service.TeamService;
import com.ideas2it.howzat.util.CommonUtil;

/**
 * Acts as Servlet, Receives request from server, 
 * Fetches informations from request and redirects to methods
 * and give response to server
 */
@Controller
public class TeamController {
    
    private TeamService teamService;
    
    private static final Logger logger = Logger.getLogger(TeamController.class);
    
    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }
   
    /**
     * Add selected players to team 
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */ 
    @RequestMapping(value = "/newTeam", method = RequestMethod.GET)
    private String createTeam(Model model) 
            throws ServletException, IOException, HowzatException {
        TeamInfo teamInfo = new TeamInfo();
        model.addAttribute("teamInfo",teamInfo);
        return "addteam";
    }
    
    /**
     * Gets input for required fields of Team,validate them, 
     * stores in a string and passes to service layer method
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/createTeam", method = RequestMethod.POST)
    private String addTeam(@ModelAttribute TeamInfo teamInfo, Model model) 
            throws ServletException, IOException, HowzatException {
        teamInfo = teamService.createTeam(teamInfo);
        model.addAttribute("teamInfo", teamInfo);
        return "addplayerstoteam";
    }
    
    /**
     * Add selected players to team 
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/addPlayersToTeam", method = RequestMethod.POST)
    private String addPlayersToTeam(
            @RequestParam("teamplayers") String[] playersId,
            @RequestParam("id") int teamId, Model model)
            throws ServletException, IOException, HowzatException {
        teamService.updateTeam(teamId, playersId, null, null);
        return "redirect:/validateTeam?teamid=" + teamId;
    }
    
    /** 
     * Display all teams 
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/viewAllTeams", method = RequestMethod.GET) 
    private String displayAllTeams(Model model) 
            throws ServletException, IOException, HowzatException {
        List<TeamInfo> teams = teamService.fetchAllTeamsAsInfo();
        model.addAttribute("teams",teams);
        return "viewallteams";
    }
    
    /** 
     * Edit operation get id number
     * fetches the needed informations and redirects to editTeam page
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/editTeam", method = RequestMethod.GET)
    private String editTeam(@RequestParam("teamid") int teamId, Model model) 
            throws ServletException, IOException, HowzatException {
        TeamInfo teamInfo = teamService.fetchTeamInfo(teamId, Boolean.FALSE);
        model.addAttribute("teamInfo",teamInfo);
        return "editteam";
    }

    /** 
     * Adds players and removes players from team
     *
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/updateTeam", method = RequestMethod.POST) 
    private String updateTeam(
            @RequestParam(required=false,name="removeplayers") String[] playersIdToRemove,
            @RequestParam(required=false,name="addplayers") String[] playersIdToAdd,
            @RequestParam(required=false,name="name") String teamName,
            @RequestParam(required=false,name="teamid") int teamId, Model model) 
            throws ServletException, IOException, HowzatException {
        teamService.updateTeam(teamId,playersIdToAdd, playersIdToRemove,   
            teamName);
        return "redirect:/validateTeam?teamid=" + teamId;
    }
    
    /** 
     * Delete's team from db
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/deleteTeam", method = RequestMethod.GET)
    private String deleteTeam(@RequestParam("teamid") int teamId, Model model)
            throws ServletException, IOException, HowzatException {
        teamService.deleteTeam(teamId);
        return "redirect:/viewAllTeams";
    }   
    
    /** 
     * Validates team for required player roles
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     */
    @RequestMapping(value = "/validateTeam", method = RequestMethod.GET)
    private String validateTeam(@RequestParam("teamid") int teamId, Model model)
            throws ServletException, IOException, HowzatException {
        TeamInfo teamInfo = teamService.fetchTeamInfo(teamId, Boolean.TRUE);
        int [] playerCounts = teamInfo.getPlayerCounts();
        model.addAttribute(Constants.BATSMAN_COUNT,playerCounts[0]);
        model.addAttribute(Constants.BOWLER_COUNT,playerCounts[1]);
        model.addAttribute(Constants.WK_COUNT,playerCounts[2]);
        model.addAttribute("teamInfo",teamInfo);
        return "viewteam";
    }     
    
    /**
     * Redirect user to error page
     * 
     * @param request - request containing values from localhost
     * @param response - response for the localhost request
     * @param String error - error to be displayed to user 
     
    private void redirectToErrorPage (
            HttpServletRequest request, HttpServletResponse response,
            String error) {
        try {
            request.setAttribute(Constants.ERROR, error);
            RequestDispatcher dispatcher = request.getRequestDispatcher(
                                            "error.jsp");            
            dispatcher.forward(request, response);
        } catch (IOException | ServletException e) {
            logger.error(Constants.ERROR_AT_REDIRECT_PAGE);
        }
    }*/
}
