package com.ideas2it.howzat.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/* Over information pojo class
 * Author J.Balakumaran
 * Since 04-06-19
 * Version 1.0.0
 */

@Entity 
@Table(name= "over")
public class Over {
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @Column(name = "runs") 
    private int runs;
    @Column(name = "boundaries_count") 
    private int boundariesCount;
    @Column(name = "sixer_count") 
    private int sixerCount;
    @Column(name = "wicket_count") 
    private int wicketCount;
    @Column(name = "wides_count") 
    private int widesCount;
    @Column(name = "byes_count") 
    private int byesCount;
    @Column(name = "leg_byes_count") 
    private int legByesCount;
    @Column(name = "over_no")
    private int overNo;
    @Column(name = "no_balls_count") 
    private int noBallsCount;
    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Set<OverDetail> overDetails;
    
    /**
     * default Constructor 
     */
    public Over() {
    }

    /**
     * @return hashcode as int.
     * @overrides.
     */
    public int hashCode() {
        return this.id;
    }    
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setRuns(int runs) {
        this.runs = runs;
    }
    
    public int getRuns() {
        return runs;
    }
    
    public void setBoundariesCount(int boundariesCount) {
        this.boundariesCount = boundariesCount;
    }
    
    public int getBoundariesCount() {
        return boundariesCount;
    }
    
    public void setSixerCount(int sixerCount) {
        this.sixerCount = sixerCount;
    }
    
    public int getSixerCount() {
        return sixerCount;
    }
    
    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }
    
    public int getWicketCount() {
        return wicketCount;
    }
    
    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }
    
    public int getWidesCount() {
        return widesCount;
    }
    
    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }
    
    public int getByesCount() {
        return byesCount;
    }
    
    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }
    
    public int getLegByesCount() {
        return legByesCount;
    }
    
    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }
    
    public int getOverNo() {
        return overNo;
    }
    
    public void setNoBallsCount(int noBallsCount) {
        this.noBallsCount = noBallsCount;
    }
    
    public int getNoBallsCount() {
        return noBallsCount;
    }
    
    public void setMatch(Match match) {
        this.match = match;
    }
    
    public Match getMatch() {
        return match;
    }
    
    public void setOverDetails(Set<OverDetail> overDetails) {
        this.overDetails = overDetails;
    }
    
    public Set<OverDetail> getOverDetails() {
        return overDetails;
    }
         
    /**
     * toString override to print whenever player object is set to print
     * String builder is used to append them into a single string
     */
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Over-ID").append("\t :")
                    .append(this.id)
                    .append("\t").append("Total runs")
                    .append(" :").append(this.runs)
                    .append("\t").append("boundries")
                    .append(" :").append(this.boundariesCount)
                    .append("\t").append("sixes")
                    .append(" :").append(this.sixerCount)
                    .append("\t").append("wickets")
                    .append(" :").append(this.wicketCount)
                    .append("\n")
                    .append("wides")
                    .append(" :").append(this.widesCount)
                    .append("\t").append("byes")
                    .append(" :").append(this.byesCount)
                    .append("\t").append("leg byes")
                    .append(" :").append(this.legByesCount)
                    .append("\t").append("No-balls")
                    .append(" :").append(this.noBallsCount)
                    .append("\t").append("match id")
                    .append(" :").append(this.match.getId());
        return stringBuilder.toString();
    }       
}
