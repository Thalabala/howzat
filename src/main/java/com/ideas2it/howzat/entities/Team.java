package com.ideas2it.howzat.entities;

import java.util.List;
import java.util.Set;

import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.common.Constants;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table(name= "team")
public class Team {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="team_Name")
    private String teamName;
    @Enumerated(EnumType.STRING)
    @Column(name="country")
    private Country country;
    @Column(name="status")
    private boolean status;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Set<Player> teamPlayers;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "match_team", joinColumns = { 
    @JoinColumn(name = "team_id") }, 
    inverseJoinColumns = { @JoinColumn(name = "match_id") })
    private Set<Match> matches;

    public Team() {
        this.status = false;
    } 
    
    /**
     * Parameterized constructor to set values to object. 
     */    
    public Team(int id,String teamName,String country, boolean status) { 
        this.id = id;
        this.teamName = teamName;
        this.status = status;
        this.country = Country.valueOf(country);    
    }
    
    
    /**
     * @param obj as Object.
     * @return boolean.
     * @overrides equals.
     */
    public boolean equals(Object object) {
        if (this == object) {
            return Boolean.TRUE;
        }
        if (null == object || object.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Team team = (Team) object;
        if ((this.id == team.getId()) && (this.teamName.equals(team.getTeamName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * @return hashcode as int.
     * @overrides.
     */
    public int hashCode() {
        return this.id;
    }  
    
    public void setId(int id) {
        this.id = id;    
    }    

    public int getId() {
        return this.id;
    }
    
    public void setTeamPlayers(Set<Player> teamPlayers) {
        this.teamPlayers = teamPlayers;    
    }    

    public Set<Player> getTeamPlayers() {
        return this.teamPlayers;
    }
    
    public void setMatches(Set<Match> matches) {
        this.matches = matches;    
    }    

    public Set<Match> getMatches() {
        return this.matches;
    }    
        
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    
    public String getTeamName() {
        return this.teamName;
    }
    
    public void setCountry(Country country) {
        this.country = country;
    }
    
    public Country getCountry() {
        return this.country;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public boolean getStatus() {
        return this.status;
    }
    
    private String getStringStatus(boolean status) {
        return status ? "Completed" : "Incomplete";
    }
    
    /**
     * toString override to print whenever team object is set to print
     * String builder is used to append them into a single string
     */
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Constants.TEAM_ID).append(" : ")
                    .append(this.id)
                    .append("\t").append(Constants.TEAM_NAME)
                    .append(" : ").append(this.teamName)
                    .append("\t").append(Constants.COUNTRY)
                    .append(" : ").append(this.country)
                    .append("\t").append(Constants.STATUS)
                    .append(" : ").append(getStringStatus(this.status));
        return stringBuilder.toString();
    }  
}
