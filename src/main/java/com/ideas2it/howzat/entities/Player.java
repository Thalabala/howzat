package com.ideas2it.howzat.entities;

import java.lang.StringBuilder;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.util.AgeUtil;
import com.ideas2it.howzat.common.Country;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Transient;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/* player information pojo class
 * player moudle alone
 * Author J.Balakumaran
 * Since 04-06-19
 * Version 1.0.0
 */


/* 
 * The class players is written as pojo
 * So it contains player details and their characteristics as private
 * so we use getter and setter functions to access them outside.
 */
@Entity 
@Table(name= "player")
public class Player {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="status")
    private boolean status;
    @Transient
    private String age;
    @Column(name="name")
    private String name;
    @Column(name="role")
    private String playerType;
    @Column(name="batting_type")
    private String battingType;
    @Column(name="bowling_type")
    private String bowelingType;
    @Column(name="date_of_birth")
    private String dateOfBirth;
    @Column(name="display_picture") 
    private String displayPicture;
    @Enumerated(EnumType.STRING)
    @Column(name="country")
    private Country country;
    @OneToOne(cascade=CascadeType.ALL, mappedBy="player")
    private Contact contact;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;

    /**
     * default Constructor 
     */
    public Player() {   
        this.status = Boolean.TRUE;
    } 
    
    /**
     * Parameterized constructor to set values to object. 
     */    
    public Player(int id,String name,String age,String dateOfBirth,
            boolean status,String country,String playerType,String battingType,
            String bowelingType, Contact contact) { 
        this.id = id;
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.status = status;
        this.country = Country.valueOf(country);
        this.playerType = playerType;
        this.battingType = battingType;
        this.bowelingType = bowelingType;
        this.contact = contact;
    }
    
    /**
     * @param obj as Object.
     * @return boolean.
     * @overrides equals.
     */
    public boolean equals(Object object) {
        if (this == object) {
            return Boolean.TRUE;
        }
        if (null == object || object.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Player player = (Player) object;
        if ((this.id == player.getId()) && (this.name.equals(player.getName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * @return hashcode as int.
     * @overrides.
     */
    public int hashCode() {
        return this.id;
    }    
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }
    
    public String getPlayerType() {
        return playerType;
    }
    
    public void setBattingType(String battingType) {
        this.battingType = battingType;
    }
    
    public String getBattingType() {
        return battingType;
    }
    
    public void setBowelingType(String bowelingType) {
        this.bowelingType = bowelingType;
    }
    
    public String getBowelingType() {
        return bowelingType;
    }
    
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    
    public void setCountry(Country country) {
        this.country = country;
    }
    
    public Country getCountry() {
        return country;
    }
    
    public void setStatus(boolean status) {
       
        this.status = status;
    }
    
    public boolean getStatus() {
       return status;
    }
    
    /**
     * @returns Active or in-active with respect to boolean status
     */
    public String getStringStatus(){
        if (status) {
           return "Active";
        }else {
            return "In-Active";
        
        }
    }
    
    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }
    
    public String getDisplayPicture() {
        return displayPicture;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    public String getAge() {
        return AgeUtil.calculateAge(this.dateOfBirth);
    }
    
    public void setContact(Contact contact){
        this.contact = contact;
    }
     
    public Contact getContact(){
        return contact;
    } 
    
    public void setTeam(Team team){
        this.team = team;
    }
     
    public Team getTeam(){
        return team;
    }
          
    /**
     * toString override to print whenever player object is set to print
     * String builder is used to append them into a single string
     */
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Contact contact = this.getContact();
            stringBuilder.append(Constants.PLAYER_ID).append("\t :")
                    .append(this.id)
                    .append("\t").append(Constants.NAME)
                    .append(" :").append(this.name)
                    .append("\t").append(Constants.AGE)
                    .append(" :").append(this.age)
                    .append("\t").append(Constants.PLAYER_STATUS)
                    .append(" :").append(getStringStatus())
                    .append("\t").append(Constants.COUNTRY)
                    .append(" :").append(getCountry())
                    .append("\n")
                    .append(Constants.PLAYER_TYPE)
                    .append(" :").append(this.playerType)
                    .append("\t").append(Constants.BOWLING_TYPE)
                    .append(" :").append(this.bowelingType)
                    .append("\t").append(Constants.BATTING_TYPE)
                    .append(" :").append(this.battingType)
                    .append("\t").append(Constants.DATE_OF_BIRTH)
                    .append(" :").append(this.dateOfBirth)
                    .append("\n")
                    .append(Constants.ADDRESS)
                    .append(" :").append(contact.getAddress())
                    .append(Constants.CONTACT_NUMBER)
                    .append(" :").append(contact.getContactNumber())
                    .append(Constants.PINCODE).append(" :")
                    .append(contact.getContactNumber());
        return stringBuilder.toString();
    }       
}
