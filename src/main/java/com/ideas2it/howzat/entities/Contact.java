package com.ideas2it.howzat.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * pojo class contains player contact fields seperte 
 * Accessed only by player pojo as one to one relationship 
 */
@Entity  
@Table(name="contact")
public class Contact {
    
    /**
     * contact information fields
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id; 
    @Column(name="address")
    private String address;
    @Column(name="contact_number")
    private String contactNumber;
    @Column(name="pincode")
    private int pincode;
    @OneToOne
    @JoinColumn(name="player_id")
    private Player player;
    
    public Contact(){
    }
    
    /**
     * Getter and Setter methods for variables
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    
    public String getContactNumber() {
        return contactNumber;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    
    public int getPincode() {
        return pincode;
    }
    
    public void setPlayer(Player player) {
        this.player = player;
    }
    
    public Player getPlayer() {
        return player;
    }    
}
