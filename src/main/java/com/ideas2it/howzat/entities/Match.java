package com.ideas2it.howzat.entities;

import java.util.List;
import java.util.Set;
import java.util.Date;

import com.ideas2it.howzat.common.Constants;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name= "matches")
public class Match {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name = "name")
    private String matchName;
    @Column(name = "location")
    private String location;
    @Column(name = "date")
    private Date date;
    @Column(name = "match_type")
    private String matchType;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "match_team", joinColumns = { 
    @JoinColumn(name = "match_id") }, 
    inverseJoinColumns = {@JoinColumn(name = "team_id") })
    private Set<Team> teams;

    public Match() {
    } 
    
    /**
     * Parameterized constructor to set values to object. 
     */    
    public Match(int id,String matchName,String location, Date date,String matchType) { 
        this.id = id;
        this.matchName = matchName;
        this.location = location;
        this.date = date;
        this.matchType = matchType;    
    }
    /**
     * @param obj as Object.
     * @return boolean.
     * @overrides equals.
     */
    public boolean equals(Object object) {
        if (this == object) {
            return Boolean.TRUE;
        }
        if (null == object || object.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Match match = (Match) object;
        if ((this.id == match.getId()) && (this.matchName.equals(match.getMatchName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * @return hashcode as int.
     * @overrides.
     */
    public int hashCode() {
        return this.id;
    }     
    
    public void setId(int id) {
        this.id = id;    
    }    

    public int getId() {
        return id;
    }
    
    public void setTeams(Set<Team> teams) {
        this.teams = teams;    
    }    

    public Set<Team> getTeams() {
        return teams;
    }
        
    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }
    
    public String getMatchName() {
        return matchName;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void setmatchType(String matchType) {
        this.matchType = matchType;
    }
    
    public String getMatchType() {
        return matchType;
    }    
    
    /**
     * toString override to print whenever match object is set to print
     * String builder is used to append them into a single string
     */
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Constants.TEAM_ID).append(" : ")
                    .append(this.id)
                    .append("\t").append(Constants.MATCH_NAME)
                    .append(" : ").append(this.matchName)
                    .append("\t").append(Constants.LOCATION)
                    .append(" : ").append(this.location)
                    .append("\t").append(Constants.DATE)
                    .append(" : ").append(this.date)
                    .append("\t").append(Constants.MATCH_TYPE)
                    .append(" : ").append(this.matchType);
        return stringBuilder.toString();
    }  
}
