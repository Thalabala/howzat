package com.ideas2it.howzat.entities;

/* User information pojo class
 * player moudle alone
 * Author J.Balakumaran
 * Since 04-06-19
 * Version 1.0.0
 */

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/* 
 * The class User is written as pojo
 * So it contains User details and their characteristics as private
 * so we use getter and setter functions to access them outside.
 */
 
@Entity
@Table(name = "user")
public class User {
    
    @Id
    @Column(name="id")
    private int id; 
    @Column(name="name")
    private String name;
    @Column(name="gender")
    private String gender;
    @Column(unique = true, name = "email_id")
    private String emailId;
    @Column(name="role")
    private String role;
    @Column(name="contact_number")
    private String contactNumber;
    @Column(name="password")
    private String password;
    @Column(name="attempts")
    private int attempts;
    @Column(name="status")
    private boolean status;
    
    public User() {
        this.setStatus(true);
    }
    
    /**
     * Parameterized constructor to set values to object. 
     */    
    public User(int id,String name,String gender, String emailId,String role,String contactNumber,String password) { 
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.emailId = emailId; 
        this.role = role;
        this.contactNumber = contactNumber;
        this.password = password;   
    }
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public String getGender() {
        return this.gender;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public void setRole(String role) {
        this.role = role;
    }
    
    public String getRole() {
        return this.role;
    }
    
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    
    public String getContactNumber() {
        return this.contactNumber;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }
    
    public int getAttempts() {
        return this.attempts;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public boolean getStatus() {
        return this.status;
    }   
}
