package com.ideas2it.howzat.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.howzat.common.DeliveryResult;

/* Over_detail (ball details) information pojo class
 * Author J.Balakumaran
 * Since 04-06-19
 * Version 1.0.0
 */
@Entity 
@Table(name= "over_detail")
public class OverDetail {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", updatable=false, nullable=false)
    private int id;
    @Column(name = "runs")
    private int runs;
    
    
    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Over over;

    @Column(name = "ball_no")
    private int ballNo;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "delivery_result")
    private DeliveryResult deliveryResult;
    
    
    @ManyToOne
    @JoinColumn(name = "batsman_id")
    private Player batsman;
    @ManyToOne
    @JoinColumn(name = "bowler_id")
    private Player bowler;
    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;
    
    /**
     * default Constructor 
     */
    public OverDetail() {
    }

    /**
     * @return hashcode as int.
     * @overrides.
     */
    public int hashCode() {
        return this.id;
    }    
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setRuns(int runs) {
        this.runs = runs;
    }
    
    public int getRuns() {
        return runs;
    }
    
    public void setBatsman(Player batsman) {
        this.batsman = batsman;
    }
    
    public Player getBatsman() {
        return batsman;
    }
    
    public void setBowler(Player bowler) {
        this.bowler = bowler;
    }
    
    public Player getBowler() {
        return bowler;
    }
    
    public void setMatch(Match match) {
        this.match = match;
    }
    
    public Match getMatch() {
        return match;
    }
    
    public void setBallNo(int ballNo) {
        this.ballNo = ballNo;
    }
    
    public int getBallNo() {
        return ballNo;
    }
    
    public void setDeliveryResult(DeliveryResult deliveryResult) {
        this.deliveryResult = deliveryResult;
    }
    
    public DeliveryResult getDeliveryResult() {
        return deliveryResult;
    }
    
    public void setOver(Over over) {
        this.over = over;
    }
    
    public Over getOver() {
        return over;
    }
    
}
