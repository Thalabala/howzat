package com.ideas2it.howzat.info;

import java.util.List;

/** playerInfo used to transfer data from service to controller 
 *  
 *  Author J.Balakumaran
 *  Since 04-06-19
 *  Version 1.0.0
 */

/* 
 * playerInfo used to transfer data from service to controller
 * so we use getter and setter functions to access them outside.
 */
public class PlayerPaginationInfo {
     
    private int noOfPages;
    private List<PlayerInfo> players;   
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    
    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }
    
    public int getNoOfPages() {
        return noOfPages;
    }
    
    public void setPlayers(List<PlayerInfo> players) {
        this.players = players;
    }
    
    public List<PlayerInfo> getPlayers() {
        return players;
    }
}
