package com.ideas2it.howzat.info;

import java.util.List;

/**
 * pojo class contains player contact fields seperte 
 * Accessed only by player pojo as one to one relationship 
 */
public class PlayMatchInfo {
    
    /**
     * matchInfo fields
     */
    private MatchInfo matchInfo;
    private TeamInfo battingTeamInfo;
    private TeamInfo bowlingTeamInfo;
    
    /**
     * Getter and Setter methods for variables
     */
    
    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }
    
    public MatchInfo getMatchInfo() {
        return matchInfo;
    }    
    
    public void setBattingTeamInfo(TeamInfo battingTeamInfo) {
        this.battingTeamInfo = battingTeamInfo;
    }
    
    public TeamInfo getBattingTeamInfo() {
        return battingTeamInfo;
    }
    
    public void setBowlingTeamInfo(TeamInfo bowlingTeamInfo) {
        this.bowlingTeamInfo = bowlingTeamInfo;
    }
    
    public TeamInfo getBowlingTeamInfo() {
        return bowlingTeamInfo;
    }
    
}
