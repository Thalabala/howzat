package com.ideas2it.howzat.info;

import java.util.List;

import com.ideas2it.howzat.common.Constants;
import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.entities.Player;
import com.ideas2it.howzat.util.AgeUtil;

/** playerInfo used to transfer data from service to controller 
 *  
 *  Author J.Balakumaran
 *  Since 04-06-19
 *  Version 1.0.0
 */

/* 
 * playerInfo used to transfer data from service to controller
 * so we use getter and setter functions to access them outside.
 */
public class PlayerInfo {
    
    private Country country;
    private int contactId;
    private int id;
    private int pincode;
    private String address;
    private String age;
    private String battingType;
    private String bowelingType;
    private String contactNumber;
    private String dateOfBirth;
    private String displayPicture;
    private String name;
    private String playerType;   
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */ 
    public void setCountry(Country country) {
        this.country = country;
    }
    
    public Country getCountry() {
        return country;
    }
    
    public void setContactId(int contactId){
        this.contactId = contactId;
    }
     
    public int getContactId(){
        return contactId;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    
    public int getPincode() {
        return pincode;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAge(String age) {
        this.age = age;
    }
    
    public String getAge() {
        return age;
    }
    
    public void setBattingType(String battingType) {
        this.battingType = battingType;
    }
    
    public String getBattingType() {
        return battingType;
    }
    
    public void setBowelingType(String bowelingType) {
        this.bowelingType = bowelingType;
    }
    
    public String getBowelingType() {
        return bowelingType;
    }
    
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    
    public String getContactNumber() {
        return contactNumber;
    }
    
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    
    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }
    
    public String getDisplayPicture() {
        return displayPicture;
    }
      
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }
    
    public String getPlayerType() {
        return playerType;
    }
}
