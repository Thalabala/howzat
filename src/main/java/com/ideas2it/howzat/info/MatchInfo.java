package com.ideas2it.howzat.info;

import java.util.List;
import java.util.Set;
import java.util.Date;

import com.ideas2it.howzat.entities.Team;
import com.ideas2it.howzat.info.TeamInfo;

/**
 * pojo class contains player contact fields seperte 
 * Accessed only by player pojo as one to one relationship 
 */
public class MatchInfo {
    
    /**
     * matchInfo fields
     */
    private int id;
    private String matchName;
    private String location;
    private Date date;
    private String matchType;
    private Set<TeamInfo> teamsSelectedForMatch;
    private List<TeamInfo> teamsForMatch;
    
    /**
     * Getter and Setter methods for variables
     */
    public void setId(int id) {
        this.id = id;    
    }    

    public int getId() {
        return id;
    }
        
    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }
    
    public String getMatchName() {
        return matchName;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void setmatchType(String matchType) {
        this.matchType = matchType;
    }
    
    public String getMatchType() {
        return matchType;
    }    
    
    public void setTeamsSelectedForMatch(Set<TeamInfo> teamsSelectedForMatch) {
        this.teamsSelectedForMatch = teamsSelectedForMatch;    
    }    

    public Set<TeamInfo> getTeamsSelectedForMatch() {
        return teamsSelectedForMatch;
    }
    
    public void setTeamsForMatch(List<TeamInfo> teamsForMatch) {
        this.teamsForMatch = teamsForMatch;
    }
    
    public List<TeamInfo> getTeamsForMatch() {
        return teamsForMatch;
    }
}
