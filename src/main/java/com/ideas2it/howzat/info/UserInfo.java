package com.ideas2it.howzat.info;

/* User information pojo class
 * player moudle alone
 * Author J.Balakumaran
 * Since 04-06-19
 * Version 1.0.0
 */


/* 
 * The class User is written as pojo
 * So it contains User details and their characteristics as private
 * so we use getter and setter functions to access them outside.
 */
public class UserInfo {
    private int id; 
    private String name;
    private String gender;
    private String emailId;
    private String role;
    private String contactNumber;
    private String password;
    private int attempts;
    
    /**
     * Getter and Setter function 
     * for all variables are written seperately 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public String getGender() {
        return this.gender;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public void setRole(String role) {
        this.role = role;
    }
    
    public String getRole() {
        return this.role;
    }
    
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    
    public String getContactNumber() {
        return this.contactNumber;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }
    
    public int getAttempts() {
        return this.attempts;
    }   
}
