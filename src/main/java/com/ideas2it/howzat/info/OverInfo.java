package com.ideas2it.howzat.info;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.howzat.entities.OverDetail;
import com.ideas2it.howzat.entities.Match;

public class OverInfo {

    private int id;
    private int count;
    private int runs;
    private int wicketCount;
    private int foursCount;
    private int sixesCount;
    private int noBallCount;
    private int widesCount;
    private int byesCount;
    private int legByesCount;
    private MatchInfo match;
    private Set<OverDetailInfo> overDetail;

    public void setId(int id) {
        this.id = id;
    }
 
    public int getId() {
        return id;
    }

    public void setCount(int count) {
        this.count = count;
    }
 
    public int getCount() {
        return count;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }
 
    public int getRuns() {
        return runs;
    }

    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }
 
    public int getWicketCount() {
        return wicketCount;
    }

    public void setFoursCount(int foursCount) {
        this.foursCount = foursCount;
    }
 
    public int getFoursCount() {
        return foursCount;
    }

    public void setSixesCount(int sixesCount) {
        this.sixesCount = sixesCount;
    }
 
    public int getSixesCount() {
        return sixesCount;
    }

    public void setNoBallCount(int noBallCount) {
        this.noBallCount = noBallCount;
    }
 
    public int getNoBallCount() {
        return noBallCount;
    }

    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }
 
    public int getWidesCount() {
        return widesCount;
    }

    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }
 
    public int getByesCount() {
        return byesCount;
    }

    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }
 
    public int getLegByesCount() {
        return legByesCount;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }
 
    public MatchInfo getMatch() {
        return match;
    }

    public void setOverDetail(Set<OverDetailInfo> overDetail) {
        this.overDetail = overDetail;
    }
 
    public Set<OverDetailInfo> getOverDetail() {
        return overDetail;
    }
}
