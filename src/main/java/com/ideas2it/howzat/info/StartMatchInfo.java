package com.ideas2it.howzat.info;

import java.util.List;

public class StartMatchInfo {

    private int bowlingTeamId;
    private int battingTeamId;
    private int firstBatsmanId;
    private int secondBatsmanId;
    private int bowlerId;
    private String firstBatsmanName;
    private String secondBatsmanName;
    private String bowlerName;
    private int matchId;
    private int overId;
    private int firstBatsmanScore;
    private int firstBatsmanFour;
    private int firstBatsmanSix;
    private int extras;
    private int secondBatsmanScore;
    private int secondBatsmanFour;
    private int secondBatsmanSix;
    private List<PlayerInfo> teamPlayers;
        
    public void setBowlingTeamId(int bowlingTeamId) {
        this.bowlingTeamId = bowlingTeamId;
    }
 
    public int getBowlingTeamId() {
        return bowlingTeamId;
    }

    public void setTeamPlayers(List<PlayerInfo> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }
 
    public List<PlayerInfo> getTeamPlayers() {
        return teamPlayers;
    }

    
    public void setBattingTeamId(int battingTeamId) {
        this.battingTeamId = battingTeamId;
    }
 
    public int getBattingTeamId() {
        return battingTeamId;
    }
        
    public void setFirstBatsmanId(int firstBatsmanId) {
        this.firstBatsmanId = firstBatsmanId;
    }
 
    public int getFirstBatsmanId() {
        return firstBatsmanId;
    }

    public void setSecondBatsmanId(int secondBatsmanId) {
        this.secondBatsmanId = secondBatsmanId;
    }
 
    public int getSecondBatsmanId() {
        return secondBatsmanId;
    }

    public void setBowlerId(int bowlerId) {
        this.bowlerId = bowlerId;
    }
 
    public int getBowlerId() {
        return bowlerId;
    }

    public void setFirstBatsmanName(String firstBatsmanName) {
        this.firstBatsmanName = firstBatsmanName;
    }
 
    public String getFirstBatsmanName() {
        return firstBatsmanName;
    }

    public void setSecondBatsmanName(String secondBatsmanName) {
        this.secondBatsmanName = secondBatsmanName;
    }
 
    public String getSecondBatsmanName() {
        return secondBatsmanName;
    }
    
    public void setBowlerName(String bowlerName) {
        this.bowlerName = bowlerName;
    }
 
    public String getBowlerName() {
        return bowlerName;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }
 
    public int getMatchId() {
        return matchId;
    }

    public void setOverId(int overId) {
        this.overId = overId;
    }
 
    public int getOverId() {
        return overId;
    }
    
    public void setExtras(int extras) {
        this.extras = extras;
    }
 
    public int getExtras() {
        return extras;
    }
    public void setFirstBatsmanScore(int firstBatsmanScore) {
        this.firstBatsmanScore = firstBatsmanScore;
    }
 
    public int getFirstBatsmanScore() {
        return firstBatsmanScore;
    }
    public void setFirstBatsmanFour(int firstBatsmanFour) {
        this.firstBatsmanFour = firstBatsmanFour;
    }
 
    public int getFirstBatsmanFour() {
        return firstBatsmanFour;
    }
    public void setFirstBatsmanSix(int firstBatsmanSix) {
        this.firstBatsmanSix = firstBatsmanSix;
    }
 
    public int getFirstBatsmanSix() {
        return firstBatsmanSix;
    }
    public void setSecondBatsmanScore(int secondBatsmanScore) {
        this.secondBatsmanScore = secondBatsmanScore;
    }
 
    public int getSecondBatsmanScore() {
        return secondBatsmanScore;
    }
    public void setSecondBatsmanFour(int secondBatsmanFour) {
        this.secondBatsmanFour = secondBatsmanFour;
    }
 
    public int getSecondBatsmanFour() {
        return secondBatsmanFour;
    }
    public void setSecondBatsmanSix(int secondBatsmanSix) {
        this.secondBatsmanSix = secondBatsmanSix;
    }
 
    public int getSecondBatsmanSix() {
        return secondBatsmanSix;
    }
    
    
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("firstBatsmanId").append("\t :")
                    .append(this.firstBatsmanId)
                    .append("\t").append("secondBatsmanId")
                    .append(" :").append(this.secondBatsmanId)
                    .append("\t").append("bowlerId")
                    .append(" :").append(this.bowlerId)
                    .append("\t").append("firstBatsmanName")
                    .append(" :").append(firstBatsmanName)
                    .append("\t").append("secondBatsmanName")
                    .append(" :").append(secondBatsmanName)
                    .append("\n")
                    .append("bowlerName")
                    .append(" :").append(this.bowlerName)
                    .append("\t").append("matchId")
                    .append(" :").append(this.matchId)
                    .append("\t").append("overId")
                    .append(" :").append(this.overId)
                    .append("\t").append("firstBatsmanScore")
                    .append(" :").append(this.firstBatsmanScore)
                    .append("\n")
                    .append("firstBatsmanFour")
                    .append(" :").append(this.firstBatsmanFour)
                    .append("firstBatsmanSix")
                    .append(" :").append(this.firstBatsmanSix)
                    .append("extras").append(" :")
                    .append(extras).append("\n")
                    .append("secondBatsmanScore")
                    .append(" :").append(this.secondBatsmanScore)
                    .append("secondBatsmanFour")
                    .append(" :").append(this.secondBatsmanFour)
                    .append("secondBatsmanSix").append(" :")
                    .append(secondBatsmanSix);
        return stringBuilder.toString();
    }  
}
