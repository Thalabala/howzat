package com.ideas2it.howzat.info;

import java.util.List;
import java.util.Set;

import com.ideas2it.howzat.common.Country;
import com.ideas2it.howzat.info.PlayerInfo;

/**
 * TeamInfo object used to get team and list informations 
 */
public class TeamInfo {
    
    /**
     * TeamInfo fields
     */
    private int id;
    private int[] playerCounts;
    private String teamName;
    private Country country;
    private boolean status;
    private List<PlayerInfo> players;
    private Set<PlayerInfo> teamPlayers;
    
    /**
     * Getter and Setter methods for variables
     */
    public TeamInfo(){
        this.status = Boolean.FALSE;
    }
    
    public void setId(int id) {
        this.id = id;    
    }    

    public int getId() {
        return this.id;
    }
    
    public void setStatus(boolean status) {
        this.status = status;    
    }    

    public boolean getStatus() {
        return this.status;
    }
    
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    
    public String getTeamName() {
        return this.teamName;
    }
    
    public void setCountry(Country country) {
        this.country = country;
    }
    
    public Country getCountry() {
        return this.country;
    }
    
    public void setPlayers(List<PlayerInfo> players) {
        this.players = players;
    }
    
    public List<PlayerInfo> getPlayers() {
        return players;
    }
    
    public void setTeamPlayers(Set<PlayerInfo> teamPlayers) {
        this.teamPlayers = teamPlayers;    
    }    

    public Set<PlayerInfo> getTeamPlayers() {
        return this.teamPlayers;
    }
    
    public void setPlayerCounts(int[] playerCounts) {
        this.playerCounts = playerCounts;    
    }    

    public int[] getPlayerCounts() {
        return this.playerCounts;
    }
    
}
