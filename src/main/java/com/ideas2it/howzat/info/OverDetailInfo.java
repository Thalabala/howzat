package com.ideas2it.howzat.info;

import com.ideas2it.howzat.common.DeliveryResult;
import com.ideas2it.howzat.info.OverInfo;
import com.ideas2it.howzat.info.MatchInfo;
import com.ideas2it.howzat.info.OverInfo;
import com.ideas2it.howzat.info.PlayerInfo;

public class OverDetailInfo {
    private int id;
    private int runs;
    private DeliveryResult deliveryResult;
    private int ballNumber;
    private PlayerInfo firstBatsman;
    private PlayerInfo secondBatsman;
    private PlayerInfo bowler;
    private MatchInfo match;
    private OverInfo overInfo;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getRuns() {
        return runs;
    }

    public void setDeliveryResult(DeliveryResult deliveryResult) {
        this.deliveryResult = deliveryResult;
    }

    public DeliveryResult getDeliveryResult() {
        return deliveryResult;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setFirstBatsman(PlayerInfo firstBatsman) {
        this.firstBatsman = firstBatsman;
    }

    public PlayerInfo getFirstBatsman() {
        return firstBatsman;
    }

    public void setSecondBatsman(PlayerInfo secondBatsman) {
        this.secondBatsman = secondBatsman;
    }

    public PlayerInfo getSecondBatsman() {
        return secondBatsman;
    }

    public void setBowler(PlayerInfo bowler) {
        this.bowler = bowler;
    }

    public PlayerInfo getBowler() {
        return bowler;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }

    public MatchInfo getMatch() {
        return match;
    }

    public void setOver(OverInfo overInfo) {
        this.overInfo = overInfo;
    }

    public OverInfo getOverInfo() {
        return overInfo;
    }
}
