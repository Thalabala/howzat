package com.ideas2it.howzat.common;

public class Constants {
   
    //Integer Constants 
    public static final int NUMERIC_ZERO = 0;
    public static final int NUMERIC_ONE = 1;
    public static final int NUMERIC_TWO = 2;     
    public static final int NUMERIC_THREE = 3;
    public static final int NUMERIC_FOUR = 4;
    public static final int NUMERIC_FIVE = 5;
    public static final int NUMERIC_SIX = 6;
    public static final int NUMERIC_SEVEN = 7;
    public static final int NUMERIC_EIGHT = 8;
    public static final int NUMERIC_NINE = 9;
    public static final int NUMERIC_TEN = 10;
    public static final int NUMERIC_HUNDRED = 100;
    public static final int NO_OF_RECORDS_PER_PAGE = 5;
    
    // Lable Constants
    
    public static final String PLAYER_ID = "id";
    public static final String NAME_CONDITION = "only Alphebets";
    public static final String DATE_OF_BIRTH = "dateOfBirth";
    public static final String DATE_FORMAT = " (DD-MM-YYYY)";
    public static final String PLAYER_ROLE = "Player Role";
    public static final String PLAYER_TYPE = "playerType";
    public static final String BATTING_TYPE = "Batting Type";
    public static final String BOWLING_TYPE = "Boweling Type" ;
    public static final String AGE = "age";
    public static final String COUNTRY = "country";
    public static final String PLAYER_STATUS = "Status";
    public static final String STATUS = "Status";
    public static final String ADDRESS = "address";
    public static final String CONTACT_CONDITION = "only 10 numbers";
    public static final String PINCODE = "Pincode";
    public static final String PINCODE_CONDITION = "only 6 numbers";
    public static final String ASTERISK_LINE = "\n***********"
                                               +"**************************";
    public static final String CHOOSE_COUNTRY = "  Select the Country :";
    public static final String YOUR_OPTION = " Your option ";
    public static final String EDIT_PLAYER = " Edit Player ";
    public static final String CREATE_TEAM = " Create Team ";
    public static final String EDIT_TEAM = " Edit Team ";
    public static final String CREATE_MATCH = " Create match ";
    public static final String EDIT_MATCH = " Edit match ";    
    public static final String CONTINUE_OR_EXIT_STATEMENT = "Enter 1 to continue"
                                           +" same operation\n Enter 0 to exit";
    public static final String STATUS_MODIFY_WARNING =" This action will make"
                                       +"player InActive \n Are you Sure?(y/n)";
    public static final String ENTER_PLAYER_ID = "\nEnter the player id ";
    public static final String WELCOME_NOTE = "\t Welcome to HowZat ";
    public static final String ENTER = "\n Enter the ";
    public static final String PLAYERS_CURD_OPTIONS = "Select any one of the "
                                                        +"Functions"
                    +"\n  1 . Add    player\n  2 . Edit    player"
                    +"\n  3 . Display    player\n  4 . Display All players"
                    +"\n  5 . Delete player\n  0 . Exit";
    public static final String EDIT_OPTIONS = "Enter the field you want to edit"
                    +"\n1.player Name\n2.Date Of Birth\n3.player Country"
                    +"\n4.player Status\n5.player Type\n6.Batting Type"
                    +"\n7.Boweling Type\n8.Address\n9.Contact Number\n10.Pincode";
    public static final String SUCCESS_MESSAGE = "*****************************"
                    +"SUCCESS*******************************";
    public static final String INVALID_MESSAGE = "INVALID INPUT" 
                                                  +"\n\nEnter a Valid Input";
    public static final String UPDATED_INFO = " Updated info ";
public static final String CREATE_PLAYER= "Create Player";
    public static String[] playerBattingType = {"Right-Hand Bastman" 
            ,"Left-Hand Batsman"};
    public static String[] playerBowlingType = {"Right-Hand FastBowler"
            , "left-Hand FastBowler", "Right-Hand SpinBowler" 
            , "left-Hand SpinBowler", "Right-Hand medium fast Bowler"
            , "Right-Hand medium fast Bowler"};
    public static String[] playerType = {"Batsman","Bowler","WicketKeeper"
            ,"All-Rounder"};   
            
    // Array Constants   
    public static final String BATTING_TYPE_OPTIONS = "\n1.Right-Hand Bastman"
                                                       +"\n2.Left-Hand Batsman";
    public static final String PLAYER_TYPE_OPTIONS = "\n1.Batsman\n2.Bowler"+
                                              "\n3.WicketKeeper\n4.All-Rounder";
    public static final String BOWLING_TYPE_OPTIONS = "\n1.Right-Hand FastBowler"
                            +"\n2.left-Hand FastBowler\n3.Right-Hand SpinBowler"
                            +"\n4.left-Hand SpinBowler"
                            +"\n5.Right-Hand medium fast Bowler"
                            + "\n6.Right-Hand medium fast Bowler";
    
    // Team constants                        
    public static final String CREATE_OPTIONS = "\nSelect from options\n"
                                                +"1.Player Administration\n"
                                                +"2.Team Administration\n"
                                                +"3.Match Administration";
    public static final String TEAM_CURD_OPTIONS = "Select any one of "
                    +"the Functions\n  1 . Add    Team\n  2 . Edit    Team"
                    +"\n  3 . Display    Team\n  4 . Display All Teams"
                    +"\n  5 . Delete Team\n  0 . Exit";
    public static final String TEAM_ID = "ID";
    public static final String TEAM = "team";
    public static final String TEAM_NAME = "team name";
    public static final String TEAM_EDIT_OPTIONS = "Select the operation "
                    +"\n1.Team Name\n2.Add players\n3.Remove player";
    public static final String ENTER_TEAM_ID = "\nEnter the Team id ";
    public static final String ENTER_TEAM_IDS = "\nEnter the Team id's ";
    public static final String ENTER_MATCH_ID = "\nEnter the match id ";  
    public static final String NOT_ENOUGH_PLAYERS = "Team should contain"
                                                      +" 11 Players ";
    public static final String NOT_ENOUGH = "Team need more";
    public static final String NOT_ENOUGH_WC = "Team must have Atleast 1" 
                                                       +"Wicket-Keeper ";
    public static final String TEAM_STATUS_COMPLETE = "Complete";
    public static final String ELEVEN_PLAYERS_ONLY = "Team Should Contain " 
                                                      + "only 11 Players";
    public static final String MATCH_NAME = "matchName";
    public static final String LOCATION = "Location";
    public static final String DATE = "date";
    public static final String MATCH_TYPE= "matchType";     
    public static final String MATCH_CURD_OPTIONS = "Select any one of "
                    +"the Functions\n  1 . Add    Match\n  2 . Edit    Match"
                    +"\n  3 . Display    Match\n  4 . Display All Matches"
                    +"\n  5 . Delete match\n  6. Add Teams\n  0 . Exit";       
    public static final String MATCH_EDIT_OPTIONS = "Select the operation "
                                            +"\n1.Match Name\n2.Match Location"
                                            +"\n3.Match Date\n4.Match Type";
    public static String[] MATCH_TYPES = {"20 over","50 overs","Test Match"};
    public static final String MATCH_TYPES_OPTIONS = "\n1.20 over\n2.50 overs"
                                                        +"\n3.Test Match";
    public static final String PAGE_OPTION = "\n Enter 1.Previous \n 2.Next "
                                                +"\n 0.Exit";
    public static final String NO_INFO= "No Informations to Show";
    public static final String VIEW_PLAYERS_OPTION= "If you want to view "
                                                       +"team players enter 1";
    public static final String CONNECTION_PROBLEM = "Connection Problem";
    public static final String ERROR_AT_VALIDATING_TEAM = "Error at validating "
                                                            +"teamID : ";
    public static final String ERROR_AT_TABLE_SIZE = "Error at total size ";
    public static final String ERROR_AT_CREATING_TEAM = "Error at creating"
                                                            +" team : "; 
    public static final String ERROR_AT_FETCHING_TEAM = "Error at fetching"
                                                            +" team :"; 
    public static final String ERROR_AT_DELETING_TEAM = "Error at creating"
                                                            +"team :";
    public static final String ERROR_AT_UPDATING_TEAM = "Error at Updating"
                                                            +"team :";
    public static final String ERROR_AT_VALIDATING_MATCH = "Error at validating"
                                                            +"match ID : ";
    public static final String ERROR_AT_CREATING_MATCH = "Error at creating"
                                                            +"match :";
    public static final String ERROR_AT_DELETING_MATCH = "Error at creating"
                                                            +"match :";
    public static final String ERROR_AT_UPDATING_MATCH = "Error at updating"
                                                            +"match :"; 
    public static final String ERROR_AT_FETCHING_MATCH = "Error at fetching"
                                                            +"match :";
    public static final String ERROR_AT_VALIDATING_PLAYER = "Error at validating"
                                                            +"player : ";
    public static final String ERROR_AT_CREATING_PLAYER = "Error at creating"
                                                            +"player :";
    public static final String ERROR_AT_DELETING_PLAYER = "Error at creating"
                                                            +"player :";
    public static final String ERROR_AT_UPDATING_PLAYER = "Error at updating"
                                                            +"player :"; 
    public static final String ERROR_AT_FETCHING_PLAYER = "Error at fetching"
                                                           +"player :";   
    public static final String ERROR_AT_USER = "Error at user ";
    public static final String ERROR_AT_CREATING_USER = "Error while creating "
                                                            +"user";
    public static final String ERROR_AT_REDIRECT_PAGE = "Error at redirecting"
                                                            +" error page";
    public static final String ERROR_AT_UPDATING_USER = "error while updating"
                                                            +"user";
    public static final String ERROR_AT_FETCHING_USER = "error while fetching"
                                                            +"user";
    public static final String ERROR_AT_MATCH_CONTROLLER = "error at dopost"
                                                           +" method in match ";   
    public static final String ERROR_AT_PLAYER_CONTROLLER = "error at dopost"
                                                           +" method in player ";
    public static final String ERROR_AT_DOGET_IN_USER = "error while doGet "
                                                              +"method in user";              
    public static final String ERROR_AT_DOPOST_IN_USER ="error while dopost "
                                                          +" method in user";     
    public static final String ACTION = "action";
    public static final String ADD = "add";
    public static final String ADD_TEAM_TO_MATCH = "addTeamToMatch";
    public static final String ADD_PLAYER_TO_TEAM = "addPlayerToTeam";
    public static final String VIEW_ALL = "viewAll";
    public static final String VIEW = "view";
    public static final String VIEW_PLAYERS = "viewPlayers";
    public static final String DELETE = "delete";
    public static final String EDIT = "edit";
    public static final String UPDATE = "update";
    public static final String SIGNUP = "signup";
    public static final String CREATE = "create";
    public static final String VALIDATE = "validate";
    public static final String LOGOUT = "logout"; 
    public static final String NAME = "name";
    public static final String EMAIL_ID = "emailId";
    public static final String ROLE = "role";
    public static final String GENDER = "gender";
    public static final String CONTACT_NUMBER = "contactNumber";
    public static final String CONTACT__NUMBER = "Contact Number";
    public static final String PASSWORD = "password";
    public static final String ERROR = "error";
    public static final String INVALID_USER = "Invalid user";
    public static final String DEACTIVATED_PROFILE = "User Account Deactivated ";
    public static final String PAGE = "page";
    public static final String PLAYERS = "players";
    public static final String NO_OF_PAGES = "noOfPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String CONTACT = "contact";
    public static final String PLAYER = "player";
    public static final String _ID = "teamId";
    public static final String MATCH_ID = "matchId";
    public static final String REMOVE_PLAYERS = "removeplayers";
    public static final String ADD_PLAYERS = "addplayers";
    public static final String TEAM_PLAYERS = "teamPlayers";
    public static final String TEAMS = "teams";
    public static final String BATSMAN_COUNT = "batsmanCount";
    public static final String BOWLER_COUNT = "bowlerCount";
    public static final String WK_COUNT = "wicketkeeperCount";
    public static final String MATCH__ID = "matchid";
    public static final String MATCH = "match";
    public static final String SELECTED_TEAMS = "selectedteams"; 
    public static final String REMOVE_TEAMS = "removeteams";
    public static final String ADD_TEAMS = "addteams";
    public static final String MATCH_TEAMS = "matchteams";
}
