package com.ideas2it.howzat.common;

/**
 * Enum class contains possible result for a ball bowled
 *
 */
public enum DeliveryResult {
    Dot,
    Ones,
    Twos,
    Three,
    Four,
    Five,
    Six,
    Wide,
    No_Ball,
    Byes,
    Leg_Byes,
    Wicket;
}
