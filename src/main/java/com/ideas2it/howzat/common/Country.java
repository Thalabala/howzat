package com.ideas2it.howzat.common;

/**
 * Enum class contains countries values
 */
public enum Country {
    INDIA, AUSTRALIA, ENGLAND, PAKISTAN, SRI_LANKA, WEST_INDIES, SOUTH_AFRICA;
}
